import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Modal } from 'bootstrap';
import { Constants } from 'src/app/objects/constans';

@Component({
  selector: 'app-reschedule-appointments',
  templateUrl: './reschedule-appointments.component.html',
  styleUrls: ['./reschedule-appointments.component.css']
})
export class RescheduleAppointmentsComponent implements OnInit {

  contactForm: FormGroup;
  messageResult: string;
  idUsuario: string;
  email: string;
  nombre: string;

  constantes = new Constants;
 
   constructor( private _formBuilder:FormBuilder, private processWebService: ProcessWebServiceService ) { 

     this.contactForm = this._formBuilder.group({
       email: new FormControl('',[Validators.required, Validators.email] ),
       name: new FormControl('', [Validators.required]),
       subject: new FormControl('',[Validators.required ]),
     }); 
     
     this.idUsuario = sessionStorage.getItem('iduser');
     this.consultDataUser();
   }
   
  ngOnInit(): void {
    window.scroll(0, 0);
  }

  consultDataUser(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema:"admin",
        tabla:"infocliente_v",
        campo:"*",
        condicion: 'idusuario = ' + this.idUsuario
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( (res) => { 
        console.log(res)
        let dataResponse = JSON.parse(res.response);
        this.nombre =  dataResponse[0].nombre + ' ' +  dataResponse[0].apellido;
        this.email = dataResponse[0].correo;
      })
  };

  sendMessage(){
    let data = {
      email: this.email,
      //username: this.nombre,
      subject: this.contactForm.get('subject').value,
      service: 2
    }    
    console.log(data);
     this.processWebService.rescheduleAppointment(data).subscribe(
      res => {
        if(res != null){
          this.messageResult ="Gracias por contactarse con nosotros, daremos respuesta a su requerimiento a través del correo registrado, tan pronto como nos sea posible.";
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      }
    ) 
  }  

}


