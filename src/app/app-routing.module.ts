import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { TriageComponent } from './triage/triage.component';
import { PerfilComponent } from './user/perfil/perfil.component';
import { ServiciosComponent } from './servicios_appodium/servicios/servicios.component';
import { ServicesAppodiumComponent } from './appodium_ervice_work/services-appodium/services-appodium.component';
import { TriageFullComponent } from './triage_full/triage-full.component';
import { ContactComponent } from './contact/contact/contact.component';
import { PricesComponent } from './prices/prices/prices.component';
import { BusinessComponent } from './business/business/business.component';
import { HabeasDataComponent } from './habeas-data/habeas-data.component';
import { CitasMedicalSoftComponent } from './citas/citas-medical-soft/citas-medical-soft.component';
import { RescheduleAppointmentsComponent } from './apointmens/reschedule-appointments/reschedule-appointments.component'


const routes: Routes = [
  { path: '',  component: HomeComponent }, 
  { path: 'home',  component: HomeComponent },
  { path: 'auth',  loadChildren: () =>   import('./auth/auth.module').then((m) => m.AuthModule), },
  //{ path: 'triage',  component: TriageFullComponent},
  { path: 'perfil',  component: PerfilComponent },
  { path: 'servicios/:id', component: ServiciosComponent },
  /* { path: 'servicios', component: ServiciosComponent }, */
  //{ path: 'servicios-appodium',  component: ServicesAppodiumComponent },
  //{ path: 'triage-full', component: TriageFullComponent },
  { path: 'contactenos',  component: ContactComponent },
  //{ path: 'costos/:id',  component: PricesComponent },
  { path: 'empresas/:id',  component: BusinessComponent },
  { path: 'habeas-data',  component: HabeasDataComponent },
  { path: 'agendar-cita',  component: CitasMedicalSoftComponent },
  { path: 'reagendamiento',  component: RescheduleAppointmentsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, 
    {
      useHash: false,
      anchorScrolling: 'enabled',
      onSameUrlNavigation: 'reload',
      scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
