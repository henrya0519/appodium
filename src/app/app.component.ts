import { Component, Inject, OnInit } from '@angular/core';
import {DOCUMENT} from "@angular/common";  
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'appodium';

  constructor(@Inject(DOCUMENT) private document) { }

  ngOnInit(): void {
    let bases = this.document.getElementsByTagName('base');

   /*  if (bases.length > 0) {
      bases[0].setAttribute('href', environment.baseHref);
    } */
  }

}
