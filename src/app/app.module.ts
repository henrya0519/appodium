import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { HeaderintComponent } from './components/headerint/headerint.component';
import { MenuComponent } from './components/menu/menu.component';
import { TriageComponent } from './triage/triage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { TestComponent } from './triage/test/test.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RegisterModalComponent } from './auth/register-modal/register-modal.component';
import { ModalResponseComponent } from './shared/modal-response/modal-response.component';
import { APP_BASE_HREF } from '@angular/common';
import { environment } from 'src/environments/environment';
import { TestMobileComponent } from './triage/test-mobile/test-mobile/test-mobile.component';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './user/perfil/perfil.component';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { ServiciosComponent } from './servicios_appodium/servicios/servicios.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { StickyBelowViewDirective } from './sticky-below-view.directive';
import { MdbCarouselModule } from 'mdb-angular-ui-kit/carousel';
import { CarouselComponent } from './shared/carousel/carousel.component';
import { ServicesAppodiumComponent } from './appodium_ervice_work/services-appodium/services-appodium.component';
import { TriageFullComponent } from './triage_full/triage-full.component';
import { TriageFormComponent } from './triage_full/triage/triage.component';
import { ContactComponent } from './contact/contact/contact.component';
import { SpinnerModule } from './shared/components/spinner/spinner.module';
import { SpinnerInterceptor } from './shared/interceptor/spinner.intenceptor';
import { PricesComponent } from './prices/prices/prices.component';
import { BusinessComponent } from './business/business/business.component';
import { HabeasDataComponent } from './habeas-data/habeas-data.component';
import { CitasMedicalSoftComponent } from './citas/citas-medical-soft/citas-medical-soft.component';
import { ListDoctorsComponent } from './list-doctors/list-doctors.component';
import { RescheduleAppointmentsComponent } from './apointmens/reschedule-appointments/reschedule-appointments.component';


const routes: Routes = [
  { path: '',  component: HomeComponent }, 
  { path: 'home',  component: HomeComponent },
  { path: 'auth',  loadChildren: () =>   import('./auth/auth.module').then((m) => m.AuthModule), },
  { path: 'triage',  component: TriageComponent },
  { path: 'perfil',  component: PerfilComponent },
  { path: 'servicios/:id', component: ServiciosComponent },
  /* { path: 'servicios', component: ServiciosComponent }, */
  { path: 'servicios-appodium',  component: ServicesAppodiumComponent },
  { path: 'triage-full', component: TriageFullComponent },
  { path: 'contactenos',  component: ContactComponent },
  { path: 'costos/:id',  component: PricesComponent },
  { path: 'empresas/:id',  component: BusinessComponent },
  { path: 'habeas-data',  component: HabeasDataComponent },
]

@NgModule({ 
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    HeaderintComponent,
    MenuComponent,
    TriageComponent,
    TestComponent,
    ModalResponseComponent,
    TestMobileComponent,
    PerfilComponent,
    ServiciosComponent,
    StickyBelowViewDirective,
    CarouselComponent,
    ServicesAppodiumComponent,
    TriageFullComponent,    
    TriageFormComponent, 
    ContactComponent, PricesComponent, BusinessComponent, HabeasDataComponent, CitasMedicalSoftComponent, ListDoctorsComponent, RescheduleAppointmentsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,    
    AuthModule,
    BrowserAnimationsModule,
    FormsModule,
    MatTableModule,
    MatRadioModule,
    MatDialogModule,
    ReactiveFormsModule,
    NgxPageScrollCoreModule,
    NgbModule,
    NgbCollapseModule,
    MdbCarouselModule,
    HttpClientModule,
    SpinnerModule,
    RouterModule.forRoot(routes,{
      anchorScrolling: 'enabled',
      useHash: false,
      onSameUrlNavigation: 'reload',
      scrollPositionRestoration: 'enabled'
    })
  ],
  entryComponents: [RegisterModalComponent, ModalResponseComponent,],
  providers:  [ {provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi:true } ],
  bootstrap: [AppComponent]
})
export class AppModule {}
