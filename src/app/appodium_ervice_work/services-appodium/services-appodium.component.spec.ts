import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesAppodiumComponent } from './services-appodium.component';

describe('ServicesAppodiumComponent', () => {
  let component: ServicesAppodiumComponent;
  let fixture: ComponentFixture<ServicesAppodiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesAppodiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesAppodiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
