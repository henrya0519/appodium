import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbSlideEvent, NgbSlideEventSource, NgbTimepickerConfig, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CAROUSEL_DATA_ITEMS } from '../../constants/carousel.const';
import { ICarouselItem } from 'src/app/shared/carousel/Icarousel-item.metadata';
import { Constants } from 'src/app/objects/constans';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Modal } from 'bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services-appodium',
  templateUrl: './services-appodium.component.html',
  styleUrls: ['./services-appodium.component.css']
})
export class ServicesAppodiumComponent implements OnInit {
  
  dataUserForm: FormGroup;     
  appointmentForm: FormGroup;   
  idUsuario = '';
  constantes = new Constants();
  mobile= false;
  idpersona: number;
  dataUserFull= false;
  documentsType: any;
  escolaridad: any;
  ocupaciones: any;
  ciudades: any;
  messageResult: string;
  user_email:string;
  typeDoc:any;
  timeAppointment: any;
  fechaNacUser: any;
  dt = new Date();
  dia = this.dt.getUTCDate();
  mes =  (this.dt.getUTCMonth() <9 )? ('0' + (this.dt.getUTCMonth()+ 1)) : ( this.dt.getUTCMonth() <9 + 1);
  ano = this.dt.getUTCFullYear();
  pago= false;
  iconButton = false;
  
  time = {hour: 13, minute: 30};
  meridian = true;
  
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddModalPayent') closeAddModalPayment: ElementRef;
  @ViewChild('closeAddModalAppointment') closeAddModalAppointment: ElementRef;

  public carouselData: ICarouselItem[] = CAROUSEL_DATA_ITEMS;

  constructor(
    private processWebService: ProcessWebServiceService, 
    private _formBuilder:FormBuilder,
    config: NgbTimepickerConfig,
    private router: Router
    )   {
    this.idUsuario = sessionStorage.getItem('iduser');
    //this.validateDataUser(0);
    this.consultDocuments();
    this.consultEscolaridad();
    this.consultOcupaciones();
    this.consultaciudades();   

  /*   config.seconds = true; */
    config.spinners = false;
   }

  ngOnInit(): void {
    window.scroll(0,0);    
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    }    

    this.dataUserForm = this._formBuilder.group({
      idtipodocumento: new FormControl('Tipo de Documento', [Validators.required]),
      documento: new FormControl('', [Validators.required]),
      nombre: new FormControl('', [Validators.required]),
      apellido: new FormControl('', [Validators.required]),
      telefono: new FormControl('', [Validators.required]),
      fechanacimiento: new FormControl('', [Validators.required]),
      /* lugarnacimiento: new FormControl('', [Validators.required]),
      idescolaridad: new FormControl('Escolaridad', [Validators.required]),
      idocupacion: new FormControl('Ocupación', [Validators.required]),
      direccion: new FormControl('', [Validators.required]),
      idciudad: new FormControl('Ciudad', [Validators.required]), */
      authorizationData: new FormControl('', Validators.required)
    })

    this.appointmentForm = this._formBuilder.group({        
      documentoCliente: new FormControl('', [Validators.required]),
      fechaCita: new FormControl('', [Validators.required]),
      horaCita: new FormControl('', Validators.required),
      nombreCliente: new FormControl('', [Validators.required]),
      telefonoCliente: new FormControl('', [Validators.required]),
      correoCliente: new FormControl('', [Validators.required]),
      motivoConsulta: new FormControl(''),
      tipoConsulta: new FormControl('-1', Validators.required)   
    });

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };
  }

  slideActivate(ngbSlideEvent: NgbSlideEvent) {
    console.log(ngbSlideEvent.source);
    console.log(ngbSlideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }

  onSlideChange(): void {
  }

  //Valida si el usario tiene la información actualizada
  validateDataUser(displayModal:any){
    
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema:"admin",
        tabla:"infocliente_v",
        campo:"*",
        condicion: 'idusuario = ' + this.idUsuario 
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( (res) => {        
        this.user_email =  JSON.parse(res.response)[0].correo
        let dataUser = JSON.parse(res.response);
        this.idpersona =  dataUser[0].idpersona;
        this.router.navigateByUrl('/agendar-cita');
      
      })
  }

  setAppointment(){    
    const element = document.getElementById('modalPayment') as HTMLElement;
    const myModalPayment = new Modal(element);
    myModalPayment.show();
  }
  
  //Valida pago en pasarela pago
  validatePayment() {
    //se registra factura de cita                 
    //modal pago éxotoso!!------------
    this.closeAddModalPayment.nativeElement.click();
    this.messageResult = 'Pago Realizado con éxito!.';
    const element = document.getElementById('myModalPay') as HTMLElement;
    const myModal = new Modal(element);
    myModal.show();
    this.pago = true;
    this.pago =  true;
    if (this.pago) {
      this.closeAddExpenseModal.nativeElement.click();
      setTimeout(() => {
        this.router.navigateByUrl('/agendar-cita');
      }, 1500);
      
      //this.registerUserMedical();
      //this.messageResult = 'Pago Realizado con éxito!.';
      /* const element = document.getElementById('myModalMedical') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show(); */
    }
  }

  appoinmentModal(){
    this.closeAddModalPayment.nativeElement.click();
    this.closeAddExpenseModal.nativeElement.click();
    this.registerUserMedical();
  }

  registerUserMedical(){
      //registro usuario en medicalsoft
      //@tipo = 1 -> registro cliente      
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 1,
      nombre:   this.appointmentForm.get('nombreCliente').value,
      documento:  this.appointmentForm.get('documentoCliente').value,
      tipoDoc: this.typeDoc,
      fechaNacimiento: this.fechaNacUser,
      telefono: '57'+ this.appointmentForm.get('telefonoCliente').value,
      correo: this.user_email
    }
    this.processWebService.medicalSoft(data)
      .subscribe(res => {
        if (res != null) {         
          this.closeAddModalPayment.nativeElement.click();          
         /* const element = document.getElementById('myModalAppointment') as HTMLElement;
          const myModalAppointment = new Modal(element);
          myModalAppointment.show(); */
        } else {
          this.messageResult = 'Se presento un error en el registro de usuario en MedicalSoft .';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      })
  }
  //Se asigna tipo de documento para consumo API medical
  setTypeDoc(){
    
    if(this.dataUserForm.get('idtipodocumento').value === 1){      
      this.typeDoc = 'CC'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 2){ 
      this.typeDoc = 'CE'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 3){
      this.typeDoc = 'PA'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 4){
      this.typeDoc = 'RC'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 5){
      this.typeDoc = 'TI'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 6){
      this.typeDoc = 'MS'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 7){
      this.typeDoc = 'AS'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 8){
      this.typeDoc = 'DNI'
    }
  }

  //Actualiza información del usuario
  updateDataUser(){ 
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 6,
      consulta: {
        ip: '192.168.0.1',
        idusuario: this.idUsuario,
        idpersona: this.idpersona,
        tipodocumento: this.dataUserForm.get('idtipodocumento').value,
        documento: this.dataUserForm.get('documento').value,
        nombre: this.dataUserForm.get('nombre').value,
        apellido: this.dataUserForm.get('apellido').value,
        telefono: this.dataUserForm.get('telefono').value,
        fechanac: this.dataUserForm.get('fechanacimiento').value,
       /*  lugarnac: this.dataUserForm.get('lugarnacimiento').value,
        escolaridad: this.dataUserForm.get('idescolaridad').value,
        ocupacion: this.dataUserForm.get('idocupacion').value,
        direccion: this.dataUserForm.get('direccion').value,
        ciudad: this.dataUserForm.get('idciudad').value, */
      }
    }        
    if (this.dataUserForm.get('authorizationData').value == true && this.dataUserForm.valid) {
      this.processWebService.execProcess(data)
        .subscribe(res => {
          //this.validateDataUser();          
          if (res.response != null) {
            this.appointmentForm.get('documentoCliente').setValue(this.dataUserForm.get('idtipodocumento').value);
            this.appointmentForm.get('nombreCliente').setValue(this.dataUserForm.get('nombre').value + ' ' + this.dataUserForm.get('apellido').value );
            this.appointmentForm.get('telefonoCliente').setValue(this.dataUserForm.get('telefono').value);
            this.appointmentForm.get('correoCliente').setValue(this.user_email);            

            this.closeAddExpenseModal.nativeElement.click();
          
            this.messageResult = "Información actualizada con éxito.";
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          } else {
            console.log('error al actualizar datos.');
          }
        })
    } else {
      this.messageResult = "Por Favor complete todo el formulario y autorice el tratamiento de datos";
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();  
    }
  
  }

  /* Data formulario */  
  consultDocuments(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "tipodocumentos",
        campo: "*",
        condicion: "cliente=1 and estado=1",
        orden:"idtipodocumento"
      }
    }  
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.documentsType = JSON.parse(res.response);
      })
  }

  consultEscolaridad(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "escolaridades",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.escolaridad = JSON.parse(res.response);
      })
  }

  consultOcupaciones(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "ocupaciones",
        campo: "*",
        orden: "nombre"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.ocupaciones = JSON.parse(res.response);
      })
  }

  consultaciudades(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "gis",
        tabla: "ciudades",
        campo: "*",
        orden: "nombre"
        //condicion: "order by nombre asc"        
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.ciudades = JSON.parse(res.response);
      })
  }

  //Registra cita en MedicalSoft 
  registerAppoinment(){
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 2, //registra cita
      documentoCliente : this.appointmentForm.get('documentoCliente').value,
      fechaCita: this.appointmentForm.get('fechaCita').value,
      horaCita: this.timeAppointment,
      nombreCliente: this.appointmentForm.get('nombreCliente').value,
      telefonoCliente: '57'+this.appointmentForm.get('telefonoCliente').value,
      correoCliente: this.appointmentForm.get('correoCliente').value,
      motivoConsulta: this.appointmentForm.get('motivoConsulta').value,
      tipoConsulta:this.appointmentForm.get('tipoConsulta').value
    }
    this.processWebService.medicalSoft(data)
      .subscribe( res => {
        this.closeAddModalPayment.nativeElement.click();
        this.closeAddExpenseModal.nativeElement.click();
        this.closeAddModalAppointment.nativeElement.click();
        if(res != null){
          this.registerInvoiceAppoinment(); 
          //this.closeAddModalAppointment.nativeElement.click();          
        } else{
          this.messageResult = 'Se presento un error al Agendar su cita. Por favor intente nuevamente.';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      } )
      this.closeAddModalAppointment.nativeElement.click();      
  }

  setTime(){
    let hora = JSON.stringify(this.appointmentForm.get('horaCita').value?.hour);
    let minutos = JSON.stringify(this.appointmentForm.get('horaCita').value?.minute);    
    this.timeAppointment = ((hora?.length > 1)? hora : '0'+hora) +':'+ ((minutos?.length > 1 )?  minutos : '0'+minutos) +':00';
  }

  //Registra factura en MedicalSoft
  //validar información de fecha vencimiento
  registerInvoiceAppoinment(){
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 3, //registro factura
      documentoCliente : this.appointmentForm.get('documentoCliente').value,
      fechaOper: this.ano+':'+this.mes+':'+this.dia, 
      fechaVen: this.appointmentForm.get('fechaCita').value,
      descripcion: 'Consulta general de diagnostico',
      totalBruto: '50000.00',
      impuestos: '6000.00',
      descuentos: '1000.00',
      totalNeto: '55000.00',
      montoPagado: '55000.00'
    }
    this.processWebService.medicalSoft(data)
      .subscribe( res => { 
        if( res != null){
          this.messageResult = 'Su cita ha sido agendada éxitosamente. Gracias por usar los servicios de Appodium';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        } else {
          /* this.messageResult = 'Se presento un error al registar la factura de su cita. Por favor intente nuevamente.';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show(); */
        }        
      });
  }

  habeasData(){
    window.open( this.constantes.getHabeasData(), "_blank");    
  }

}
