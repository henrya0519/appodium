import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';  

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { RegisterModalComponent } from './register-modal/register-modal.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RecoveryPassComponent } from './recovery-pass/recovery-pass.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AuthComponent,
    RegisterModalComponent,
    RecoveryPassComponent
  ],
  imports: [
    AuthRoutingModule,
    BrowserModule,   
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
})
export class AuthModule { }