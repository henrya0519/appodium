import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { sha512 } from 'js-sha512';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { LoginService } from 'src/app/services/login-service.service';
import { RequestProcess } from 'src/app/objects/request';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Constants } from 'src/app/objects/constans';
import { Router } from '@angular/router';
import { Modal } from 'bootstrap';
import { Location } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  LoginForm: FormGroup;
  userObject: any = {
    user: '',
    upass: ''
  };
  constantes = new Constants();
  messageResult: string;


  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  constructor(
    private _formBuilder:FormBuilder, 
    private processWebService: ProcessWebServiceService,
    private _router: Router,
    private loginService: LoginService,
    private _location: Location ) {

      this.LoginForm = this._formBuilder.group({
        email: new FormControl('',[Validators.required, Validators.email] ),
        password: new FormControl('',[Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$")]),
      });

    }

  ngOnInit(): void {
    window.scroll(0,0);  
  }
  
  get correoNoValido() {
    return this.LoginForm.get('email').invalid && this.LoginForm.get('email').touched;
  }
  get passNoValido() {
    return this.LoginForm.get('password').invalid && this.LoginForm.get('password').touched;
  }
  
  getPassRequired(value){
    return this.LoginForm.get(value);
  }

  goBack(){
    this._location.back();
  }

  login(){
    if(!this.LoginForm.valid){
      this.userObject.user = this.LoginForm.get('email').value;
      this.userObject.upass = sha512(this.LoginForm.get('password').value);      
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 5,
        consulta: {
          ip: "192.168.3.127",
          cuenta: this.userObject.user,
          clave: this.userObject.upass,
        }
      }
      this.processWebService.execProcess(data)      
        .subscribe( res => {
          if( res.idresponse == 1){
            this.loginService.isLogged(JSON.parse(res.response).idusuario, JSON.parse(res.response).idpersona);
            this._router.navigateByUrl('/agendar-cita');
          }else{
            sessionStorage.setItem( 'isLogin', "false");
            //console.log(JSON.parse(res.message).nombre);
            
            this.messageResult = "Por favor verifique la información ingresada";
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          }
            
        })
    }
  }

}

