import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { sha512 } from 'js-sha512';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { LoginService } from 'src/app/services/login-service.service';
import { RequestProcess } from 'src/app/objects/request';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Constants } from 'src/app/objects/constans';
import { Router } from '@angular/router';
import { Modal } from 'bootstrap';
import {RouterModule} from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-recovery',
  templateUrl: './recovery-pass.component.html',
  styleUrls: ['./recovery-pass.component.css']
})

export class RecoveryPassComponent implements OnInit {

  recoveryForm: FormGroup;
  userObject: any = {
    user: '',
    upass: ''
  };
  constantes = new Constants();
  messageResult: string;
  iduser: number;
  isOtp= false;
  messageSuccess = 'Se ha enviado un código OTP al e-mail suministrado. Por favor verifique su correo electrónico';

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  constructor(
    private _formBuilder:FormBuilder, 
    private processWebService: ProcessWebServiceService,
    private _router: Router,
    private loginService: LoginService,
    private _location: Location ) { }

  ngOnInit(): void {
    this.iduser = this.loginService.idUsuario;
    window.scroll(0,0);

    this.recoveryForm = this._formBuilder.group({
      email: new FormControl('',[Validators.required, Validators.email ],),
      password: new FormControl('',[Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$")]),
      otp: new FormControl('', [Validators.required])
    });
  }
  
  get correoNoValido() {
    return this.recoveryForm.get('email').invalid && this.recoveryForm.get('email').touched;
  }
  get passNoValido() {
    return this.recoveryForm.get('password').invalid && this.recoveryForm.get('password').touched;
  }
  getPassRequired(value){
    return  this.recoveryForm.get(value);
  }

  recoveryPassOtp(){
    this.userObject.user = this.recoveryForm.get('email').value;
    
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 2,
      consulta: {
        ip: "192.168.3.127",
        correo: this.userObject.user,
      }
    }
    this.processWebService.recoveryPass(data)
    //this.processWebService.execProcess(data)
    .subscribe(res => {
      if (res.success == true) {
        this.isOtp = true;
        this.messageResult = (res.statusMessage == '')? this.messageSuccess: res.statusMessage;
        const element = document.getElementById('myModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();
      }else{
        this.messageResult = "Error al enviar la petición, por favor intente de nuevo.";
        const element = document.getElementById('myModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();    
      }
    })

   /*  this.processWebService.execProcess(data)
    .subscribe(res => {
      if (res.idresponse === 1) {
        this.isOtp = true;        
      }else{
        this.messageResult = JSON.parse(res.message).nombre;
        const element = document.getElementById('myModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();    
      }
    }) */
  }

    recoveryPass(){
    if(!this.recoveryForm.valid){

      this.userObject.user = this.recoveryForm.get('email').value;
      this.userObject.upass = sha512(this.recoveryForm.get('password').value);      
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 1,
        consulta: {
          schema:"admin",
          tabla:"infocliente_v",
          campo:"*",
        }
      }
      this.processWebService.execProcess(data)
      .subscribe( res => {
        let response = JSON.parse(res.response)
        if( res.idresponse == 1){
          let dataUser = response.filter( el => {
           return  el.correo == this.userObject.user
         })
          if (dataUser != null || dataUser != undefined) {
            let data = {
              cuenta: this.constantes.getCue(),
              clave: this.constantes.getClave(),
              proceso: 7,
              consulta: {
                ip: "192.168.3.127",
                idusuario: dataUser[0]?.idusuario,
                clave: this.userObject.upass,
              }
            }
            this.processWebService.execProcess(data)
              .subscribe(res => {
                if (res.idresponse == 1) {
                  this._router.navigateByUrl('/login');
                } else {
                  this.messageResult = JSON.parse(res.message).nombre;
                  const element = document.getElementById('myModal') as HTMLElement;
                  const myModal = new Modal(element);
                  myModal.show();
                }

              })
          }
        }
        else{
          this.messageResult = JSON.parse(res.message).nombre;
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();    
        }          
      })    
    }
  }

  validateRecovery() {
    if (!this.recoveryForm.valid) {
      this.userObject.otp = this.recoveryForm.get('otp').value;
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 3,
        consulta: {
          ip: "192.168.3.127",
          correo: this.userObject.user,
          otp: this.userObject.otp,
        }
      }
      this.processWebService.execProcess(data)
        .subscribe(res => {
          if (res.idresponse === 1) {
            this.recoveryPass()
          }else{
            this.messageResult = JSON.parse(res.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          }

        })
    }
  }


  //se consulta si el usuario ya tiene cuenta en la plataforma
  consultUser(){
    this.userObject.user = this.recoveryForm.get('email').value;
    this.userObject.upass = sha512(this.recoveryForm.get('password').value);      
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema:"admin",
        tabla:"infocliente_v",
        campo:"*",
        condicion: "correo = ''" + this.userObject.user +"''",
      }
    }
    console.log(data);
    this.processWebService.execProcess(data)
        .subscribe((res:any) => {
          if(res.status == true){
            this.recoveryPassOtp()
          }
          else {
            this.messageResult = 'Usuario no encontrado en la plataforma.';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          } 
        })
    }

    goBack(){
      this._location.back();
    }

}

