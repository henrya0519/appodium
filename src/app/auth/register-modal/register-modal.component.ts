import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css']
})
export class RegisterModalComponent implements OnInit {

  validatingForm: FormGroup;

  constructor( public dialogRef: MatDialogRef<RegisterModalComponent>, 
    @Inject(MAT_DIALOG_DATA) public message: string)
   { } 
   
  ngOnInit(): void {

  }


}
