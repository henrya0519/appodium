import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { sha512 } from 'js-sha512';
import { Constants } from 'src/app/objects/constans';
import { Router } from '@angular/router';
import { Modal } from 'bootstrap';
import { Location } from '@angular/common';
import { PasswordValidator } from './password.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  RegisterForm: FormGroup;
  userObject: any = {
    user: '',
    upass: '',
    otp: ''
  };
  isOtp = false;
  constantes = new Constants();
  messageResult:string;
  declaracionValidate: boolean;

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  constructor(
    private _formBuilder: FormBuilder,
    private processWebService: ProcessWebServiceService,
    private _router: Router,
    private _location: Location
  ) { }

  ngOnInit(): void {
    window.scroll(0,0);
    this.RegisterForm = this._formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      //password: new FormControl('', [Validators.required,  Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$")]),
      password: new FormControl('', /* [Validators.required, PasswordValidator.strong] */
       Validators.compose([
        Validators.minLength(8),
        Validators.required,
        Validators.pattern(
          /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&])(?=.{8,})/
        ),
        Validators.minLength(8),
       /*  Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}$") */
      ])),
      otp: new FormControl('', [Validators.required]),
      authorizationData: new FormControl('',[Validators.required])
    })
  }

  get correoNoValido() {
    return this.RegisterForm.get('email').invalid && this.RegisterForm.get('email').touched;
  }
  get passNoValido() {
    return this.RegisterForm.get('password').invalid && this.RegisterForm.get('password').touched && this.RegisterForm.get('password').errors.PasswordValidator;
  }
  getPassRequired(value){
    return this.RegisterForm.get(value);
  }

  goBack(){
    this._location.back();
  }

  isDeclaracionValidate(){
    if( this.RegisterForm.get('email').value != null && this.RegisterForm.get('email').valid && this.passNoValido != false && this.RegisterForm.get('authorizationData').value == true ){
      this.declaracionValidate = true;
    } else { this.declaracionValidate = false};
  }

  registerOtp(){
    this.userObject.user = this.RegisterForm.get('email').value;
    this.userObject.upass = sha512(this.RegisterForm.get('password').value);
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 2,
      consulta: {
        ip: "192.168.3.127",
        correo: this.userObject.user,
      }
    }
    this.processWebService.otp(data)
    //this.processWebService.execProcess(data)
    .subscribe(res => {
      if (res.success == true) {
        this.isOtp = true;
        this.messageResult = 'Se ha enviado un código OTP al e-mail suministrado. Por favor verifique su correo electrónico';
        const element = document.getElementById('myModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();
      }else{
        this.messageResult = "Error al enviar la petición, por favor intente de nuevo.";
        const element = document.getElementById('myModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();    
      }
    })
  }

  register() {
    this.userObject.user = this.RegisterForm.get('email').value;
    this.userObject.upass = sha512(this.RegisterForm.get('password').value);

    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 4,
      consulta: {
        ip: "192.168.3.127",
        correo: this.userObject.user,
        clave: this.userObject.upass,
      }
    }    
    
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res.idresponse === 1) {
          sessionStorage.setItem('iduser',  JSON.parse(res.response).idusuario);
          sessionStorage.setItem( 'isLogin', "true");
          this._router.navigateByUrl('/agendar-cita');
        }else{
          this.messageResult = JSON.parse(res.message).nombre;
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();    
        }
      })
  }

  validateRegister() {

    if (!this.correoNoValido  && this.RegisterForm.get('password').value != '') {
      this.userObject.otp = this.RegisterForm.get('otp').value;
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 3,
        consulta: {
          ip: "192.168.3.127",
          correo: this.userObject.user,
          otp: this.userObject.otp,
        }
      }
      //console.log(data);
      this.processWebService.execProcess(data)
        .subscribe(res => {
          if (res.idresponse === 1) {
            this.register();
          }else{
            this.messageResult = JSON.parse(res.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          }
        })
    }
  }
  
  habeasData(){
    window.open(this.constantes.getHabeasData(), "_blank");    
  }

  validateForm(){
   /*  console.log(this.RegisterForm.valid);//false
    console.log(this.RegisterForm.get('email').valid); //true
    console.log(this.RegisterForm.get('password').valid); //false
    console.log(this.declaracionValidate); //true
    console.log(this.RegisterForm.get('password').errors.pattern);
    console.log(this.declaracionValidate); */
  }

  consultUser(){
    /* console.log(this.RegisterForm.get('password').valid)
       console.log(this.RegisterForm.get('password').errors.pattern.valid) */
        
    if(this.declaracionValidate && this.RegisterForm.get('password').value != ''){
      this.userObject.user = this.RegisterForm.get('email').value;
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 1,
        consulta: {
          schema: 'admin',
          tabla: 'infocliente_v',
          campo: '*',
          condicion: "correo = ''" + this.userObject.user +"''",
        }
      }
      this.processWebService.execProcess(data)
        .subscribe((res:any) => {
          if(res.status == true){
            this.messageResult = 'El correo ya se encuentra registrado.';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          } else {
            this.registerOtp()
          }
        })
    } else {
      this.messageResult = 'Por favor verifique que todos los campos esten debidamente diligenciados.';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();    
    }
    
   
  }
}
