import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavigateService } from 'src/app/services/navigate.service';
import {Constants} from 'src/app/objects/constans';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.css']
})
export class BusinessComponent implements OnInit {

  constants = new Constants();
  mobile= false;
  serviceType: any;
  service: {id: number};
  posCovid= false;
  psicolaboral= false;
  doblePresencia= false;
  iconButton = false;
  habeas = this.constants.habeasData;
  
  constructor(private rutaActiva: ActivatedRoute, private toSection: NavigateService) { }

  ngOnInit(): void {
    this.service = {
      id: this.rutaActiva.snapshot.params.id
    }
    this.rutaActiva.params.subscribe((params: Params) => {
      this.service.id = params.id;
    });
    
    this.view(this.service.id);

    window.scroll(0, 0);
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    };

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };

  }

  view(serviceType){
    if (serviceType == 1) {
      this.posCovid = true;
      this.psicolaboral = this.doblePresencia = false;
    } else if (serviceType == 2) {
      this.psicolaboral = true;
      this.posCovid = this.doblePresencia = false;
    } else {
      this.doblePresencia = true;
      this.psicolaboral = this.posCovid = false;
    }
  }

  navigateToSection(){
    this.toSection.setSection("empresas");
  }

}
