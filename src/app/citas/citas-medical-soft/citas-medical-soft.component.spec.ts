import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitasMedicalSoftComponent } from './citas-medical-soft.component';

describe('CitasMedicalSoftComponent', () => {
  let component: CitasMedicalSoftComponent;
  let fixture: ComponentFixture<CitasMedicalSoftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitasMedicalSoftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitasMedicalSoftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
