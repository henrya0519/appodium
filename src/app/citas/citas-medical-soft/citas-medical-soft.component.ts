import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TypeDoctorService } from 'src/app/services/type-doctor.service';
import {Constants} from 'src/app/objects/constans';

@Component({
  selector: 'app-citas-medical-soft',
  templateUrl: './citas-medical-soft.component.html',
  styleUrls: ['./citas-medical-soft.component.css']
})
export class CitasMedicalSoftComponent implements OnInit {

  constants = new Constants();

  mobile = false;
  iconButton = false;
  doctorSelected = false;

  doctor1 = false
  doctor2 = false
  doctor3 = false
  doctor4 = false
  doctor5 = false
  isLogged =  false;
  doctor = '';
  docGeneral = true;
  docMagister = true;
  habeas = this.constants.habeasData;

  constructor(private router: Router, private doctorType: TypeDoctorService) { }

  ngOnInit(): void {
    window.scroll(0,0);    
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    }  
    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };
    
    (sessionStorage.getItem('isLogin') == 'true') ? this.isLogged = true : this.isLogged = false;
  /*   setTimeout( () => {
     
  
    }, 2000);
 */

    this.doctor = this.doctorType.getDoctor();
    if(this.doctor == 'general'){
      this.docMagister = false
    } else if(this.doctor != undefined && this.doctor != '') {
      this.docGeneral = false;    
    }    
  }

  ngOnDestroy():void {
    this.doctorType.setDoctor('');
  }

  doctorSelect(doctor:any){    
    //this.router.navigateByUrl('/');
    this.doctorSelected = true;

    switch(doctor){
      case 1:
        this.doctor1 = true;
        this.doctor5 = this.doctor2 = this.doctor3 = this.doctor4 = false;
      break
      case 2:
        this.doctor2 = true;
        this.doctor1 = this.doctor5 = this.doctor3 = this.doctor4 = false;
      break
      case 3:
        this.doctor3 = true;
        this.doctor1 = this.doctor2 = this.doctor5 = this.doctor4 = false;
      break
      case 4:
        this.doctor4 = true;
        this.doctor1 = this.doctor2 = this.doctor3 = this.doctor5 = false;
      break
      case 5:
        this.doctor5 = true;
        this.doctor1 = this.doctor2 = this.doctor3 = this.doctor4 = false;
      break
    }
  }

  backToList(){
    this.doctorSelected = false;
  }
}
