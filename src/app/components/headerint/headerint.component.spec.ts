import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderintComponent } from './headerint.component';

describe('HeaderintComponent', () => {
  let component: HeaderintComponent;
  let fixture: ComponentFixture<HeaderintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
