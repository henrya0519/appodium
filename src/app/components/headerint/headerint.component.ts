import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import { Constants } from 'src/app/objects/constans';

@Component({
  selector: 'app-headerint',
  templateUrl: './headerint.component.html',
  styleUrls: ['./headerint.component.css']
})
export class HeaderintComponent implements OnInit {
  @Output() scroll: EventEmitter<any> = new EventEmitter();
  userLogged = "";
  isCollapsed = false;
  toggleNavbar =  false;

  private fragment: string;
  constantes = new Constants();

  constructor(
    private _router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userLogged = sessionStorage.getItem('isLogin');    
    this.route.fragment.subscribe(fragment => {this.fragment = fragment; });
  }

  ngAfterViewInit(): void {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }
  
  closeMenuH(){
    this.toggleNavbar = false;
  }
  /*
  scrollToAbout() {
    this.scroll.emit(true);
  }

  logout(){
    sessionStorage.removeItem("isLogin");
    this.userLogged = 'false';
    this._router.navigateByUrl('/home');
    sessionStorage.removeItem('iduser');
  }

  scrollToSectionAbout(){
    window.scroll(0, this.constantes.about )
  }
  scrollToSectionService(){
    window.scroll(0, this.constantes.service )
  }
  scrollToSectionHowWork(){
    window.scroll(0, this.constantes.howWork )
  }
  scrollToSectionTeam(){
    window.scroll(0, this.constantes.team )
  }
  scrollToSectionPrices(){
    window.scroll(0, this.constantes.prices )
  }
  scrollToSectionBusiness(){
    window.scroll(0, this.constantes.business )
  }
  */
}
