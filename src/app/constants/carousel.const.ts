import {ICarouselItem} from '../shared/carousel/Icarousel-item.metadata'

export const CAROUSEL_DATA_ITEMS: ICarouselItem[] = [
    {
        id: 1,
        title: {
            first: 'Bienvenido(a)',
            secondMobile: 'A UN CLICK DE EMPODERAR TU MENTE!!!',
            second: 'A UN CLICK DE EMPODERAR TU MENTE!!!', 
        },
        subtitle: '',
        link: '/',
        image: 'assets/img/appodium/7.jpg'
    },
    {
        id: 2,
        title: {
            first: '',
            secondMobile: 'Un saludo de acogida para el inicio de esta experiencia de atención en nuestro consultorio virtual. ahora que decidiste que era momento para dejarte apoyar, te contaremos como responder a esa necesidad de ayuda que reune la rigurosidad de la atencón psicologíca y las ventajas de la virtualidad, mínimizando las barreras para ocuparte de tu salud mental.',
            //secondMobile: 'HOLA!!! Un saludo de acogida para el inicio de esta experiencia de atención en nuestro consultorio virtual. la telepsicología reune la rigurosidad de la atención clínica, aprovechando las ventajas de la virtualidad, mínimizando las barreras de tiempo y espacio para ocuparte de tu salud mental.',
            second: 'Un saludo de acogida para el inicio de esta experiencia de atención en nuestro consultorio virtual. ahora que decidiste que era momento para dejarte apoyar, te contaremos como responder a esa necesidad de ayuda que reune la rigurosidad de la atencón psicologíca y las ventajas de la virtualidad, mínimizando las barreras para ocuparte de tu salud mental.', 
        },
        subtitle: '',
        link: '/',
        image: 'assets/img/appodium/1-hr.jpg'
    },
    {
        id: 3,
        title: {
            first: '',
            secondMobile: 'La psicología no puede decirle a la gente cómo deben vivir sus vidas. Sin embargo, puede proporcionarles los medios para efectuar el cambio personal y social!!! (Albert Bandura)',
            second: 'La psicología no puede decirle a la gente cómo deben vivir sus vidas. Sin embargo, puede proporcionarles los medios para efectuar el cambio personal y social!!! (Albert Bandura)',
        },
        subtitle: '',
        link: '/',
        image: 'assets/img/appodium/2-hr.jpg'
    },
    {
        id: 4,
        title: {
            first: '',
            second: 'Actúa como si vivieras por segunda vez y la primera lo hubieras hecho tan desacertadamente como estás a punto de hacerlo ahora!!! (Victor Frankl)',
            secondMobile: 'Actúa como si vivieras por segunda vez y la primera lo hubieras hecho tan desacertadamente como estás a punto de hacerlo ahora!!! (Victor Frankl)'
        },
        subtitle: '',
        link: '/',
        image: 'assets/img/appodium/3-hr.jpg'
    },
    {
        id: 5,
        title: {
            first: '',
            second: 'Nadie puede convencer a otro que cambie. Cada uno de nosotros custodia una puerta del cambio que sólo puede abrirse desde adentro!!! (Virginia Satir)',
            secondMobile: 'Nadie puede convencer a otro que cambie. Cada uno de nosotros custodia una puerta del cambio que sólo puede abrirse desde adentro!!! (Virginia Satir)'
        },
        subtitle: '',
        link: '/',
        image: 'assets/img/appodium/4-hr.jpg'
    }

];