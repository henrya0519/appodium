import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Modal } from 'bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  messageResult: string;

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddExpenseModalSuccess') closeAddModalPayment: ElementRef;

  constructor(
    private _formBuilder: FormBuilder,
    private processWebService: ProcessWebServiceService,
    private _router: Router) {
    this.contactForm = this._formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      name: new FormControl('', [Validators.required]),
      subject: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    window.scroll(0, 0);
  }

  sendMessage() {
    if (this.contactForm.valid) {
      let data = {
        username: this.contactForm.get('name').value,
        email: this.contactForm.get('email').value,
        subject: this.contactForm.get('subject').value,
        service: 1
      }      
      this.processWebService.sendMail(data).subscribe(
       res => {
         if(res != null){
           this.messageResult = "Gracias por contactarse con nosotros, daremos respuesta a su requerimiento a través del correo registrado, tan pronto como nos sea posible.";
           const element = document.getElementById('myModalSuccess') as HTMLElement;
           const myModal = new Modal(element);
           myModal.show();
           this.contactForm.get('name').setValue('');
           this.contactForm.get('email').setValue('');
           this.contactForm.get('subject').setValue('');
         }
       }); 
    } else {
      this.messageResult = "Por favor verifique que todos los campos esten debidamente diligenciados.";
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }
  }

  redirectHome() {
    this._router.navigateByUrl('/home');
  }

}
