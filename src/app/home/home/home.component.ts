import { Component, OnInit, AfterViewInit, EventEmitter, Output, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { PageScrollService } from 'ngx-page-scroll-core';
import { NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { CAROUSEL_DATA_ITEMS } from '../../constants/carousel.const';
import { ICarouselItem } from 'src/app/shared/carousel/Icarousel-item.metadata';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NavigateService} from '../../services/navigate.service';
import { TypeDoctorService } from 'src/app/services/type-doctor.service';
import {Constants} from 'src/app/objects/constans';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements AfterViewInit, OnInit {

  @Output() scroll: EventEmitter<any> = new EventEmitter();  
  
  public carouselData: ICarouselItem[] = CAROUSEL_DATA_ITEMS;
  private fragment: string;

  collapseNav = true;
  mobile = false;
  userLogged = "";
  section: {sect: string}
  toSection: string
  iconButton= false;
  constants = new Constants();
  habeas = this.constants.habeasData;
  
  constructor( 
    private route: ActivatedRoute,
    private navigate: NavigateService,
    private doctorSrvc: TypeDoctorService,
    private router: Router  ) { }
  
  ngOnInit(): void {
    window.scroll(0, 0);
    this.userLogged = sessionStorage.getItem('isLogin');
    this.route.fragment.subscribe(fragment => {this.fragment = fragment;})
    if (this.fragment != undefined) {
      setTimeout(() => {
        //console.log(document.documentElement.scrollTop )      
        document.documentElement.scrollTop = document.getElementById(this.fragment).offsetTop;
      }, 500);
    }    
   
    if (window.screen.width <= 992) { //768px portrait
      this.mobile = true;
      //console.log(this.mobile, window.screen.width)
    };

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    }
    this.toSection = this.navigate.getSection();   
  }

  ngAfterViewInit() {
    /* if(this.toSection != undefined){
       this.goto(this.toSection);
    } */
  /*   try{
      console.log(this.fragment)
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch(e){} */
  }

  scrollToAbout() {
    this.scroll.emit(true);
  }

  goto(section) {
    setTimeout(() => {
      //console.log(document.documentElement.scrollTop )
      document.documentElement.scrollTop = document.getElementById(section).offsetTop;
    }, 500);
/*     switch(section){
      case 1:
        document.getElementById('quienesSomos').scrollIntoView({behavior: 'smooth'});
        break;
      case 2:
        document.getElementById('ofrecemos').scrollIntoView({behavior: 'smooth'});
        break;
      case 3:
        document.getElementById('funciona').scrollIntoView({behavior: 'smooth'});
        break;
      case 4:
        document.getElementById('team').scrollIntoView({behavior: 'smooth'});
        break;

    } */
    
  }

  collapseMenu() {
    this.collapseNav = !this.collapseNav;
  }

  logout(){
    sessionStorage.removeItem("isLogin");
    this.userLogged = 'false';
  }

  slideActivate(ngbSlideEvent: NgbSlideEvent) {
    console.log(ngbSlideEvent.source);
    console.log(ngbSlideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }

  onSlideChange(): void {
  }

  setTypeDoctor(doctor: number){
    if(doctor == 1){
      this.doctorSrvc.setDoctor('general');
      this.router.navigateByUrl('/agendar-cita');      
    }

    if(doctor == 2){
      this.doctorSrvc.setDoctor('magister');
      this.router.navigateByUrl('/agendar-cita');
    }
  }
}
