export class Constants {
    /*--Develop--
    private url ='http://localhost:8080/api/appodium';*/

    /*pre prod 
    private url ='http://200.13.195.199:8094/api/appodium';*/

    /* Prod */
    private url ='https://appodium.com.co';
    
    private resultTriage = this.url+':8094/api/appodium/triage';   
    private contact = this.url+':8094/api/appodium/emailContact';
    private endpoint=this.url+':8094/api/appodium/ejecutor'; 
    private otp = this.url+':8094/api/appodium/otp';
    private rescheduleAppointment = this.url+':8094/api/appodium/reschedule-appointment';
    private recoveryPass = this.url+':8094/api/appodium/recoveryPass';

    private cue = 'appweb';
    private clave= '5bfbcb9da037a100be728c5969e37f07c8e5d0c1930f1d44f96a6688d8faa0dd9ea97abed361d70a7efbcadc4f9c206909812792d36f5e051e8e3ce1232b7f3d';    
    
    //medicalsoft
    private endPointMedical = "https://medicalsoftplus.com/apiMedical.php";
    private tokenMedical = "PGNvNTcxPg==";
    private pass = "b78032dee51ab701c5aa9314734b8c7f";

    //coordinates sections pages
    about = 652.7999877929688;
    service = 1386.4000244140625;
    howWork = 2156;
    team =  2927.199951171875;
    prices = 4589.60009765625;
    business = 5324.7998046875;
    habeasData = this.url+'/#/habeas-data'

    constructor() { }

    public getCue(): string {
        return this.cue;
    }

    public getHabeasData(): string {
        return this.habeasData;
    } 
    
    public getClave(): string{
         return this.clave;
    }

    public getEndpoint(): string {
        return this.endpoint;
    }

    public getOTP(): string {
        return this.otp;
    }

    public getRecoveryPass(): string {
        return this.recoveryPass;
    }

    public getResultTriage(): string {
        return this.resultTriage;
    }

    public getContact(){
        return this.contact;
    }

    public getEndpointMedical(): string {
        return this.endPointMedical;
    }

    public getTokenMedical(): string{
        return this.tokenMedical;
    }

    public getPassMedical(): string{
        return this.pass;
    }

    public getRescheduleAppointment(){
        return this.rescheduleAppointment;
    }
}