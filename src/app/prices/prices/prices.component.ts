import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavigateService } from '../../services/navigate.service';
import {Constants} from 'src/app/objects/constans';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.css']
})
export class PricesComponent implements OnInit {

  constants = new Constants();

  mobile= false;
  serviceType: any;
  service: {id: number};
  costos= false;
  planes= false;
  programa= false; 
  iconButton = false;
  habeas = this.constants.habeasData;

  constructor(private rutaActiva: ActivatedRoute,  private toSection: NavigateService) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.service = {
      id: this.rutaActiva.snapshot.params.id
    }
    this.rutaActiva.params.subscribe((params: Params) => {
      this.service.id = params.id;
    });

    this.viewPrice(this.service.id);
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
      //console.log(this.mobile, window.screen.width)
    };

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };
  }

  viewPrice(serviceType) {

    if (serviceType == 1) {
      this.costos = true;
      this.planes = this.programa = false;
    } else if (serviceType == 2) {
      this.planes = true;
      this.costos = this.programa = false;
    } else {
      this.programa = true;
      this.planes = this.costos = false;
    }
  }

  navigateToSection(){
    this.toSection.setSection("costos");
  }

}
