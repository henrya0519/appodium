import { Injectable } from '@angular/core';
import { ProcessWebServiceService } from './process-web-service.service';
import { Constants } from '../objects/constans';


@Injectable({
    providedIn: 'root'
  })

  export class LoginService {

    headerOptions: any = null;
    _isLoggedIn: boolean = false;
    userLogged: string;
    userPass: string;
    cuenta:string;
    clave:string;
    idUsuario : number;
    idpersona: number;
    
    constants = new Constants();

    constructor(
     private _processWebService:  ProcessWebServiceService,
    ) {           
     
    }
  
    login(peticion: any) {
      peticion.clave = this.constants.getClave();
      peticion.cuenta = this.constants.getCue();
      
     return new Promise((resp, reject) => { 
        this._processWebService.execProcess(peticion)
        .subscribe((res) => {
          if (res.idresponse === 1 && res.response) {
            const data = JSON.parse(res.response['dataresponse']);  
            this._isLoggedIn = true;    
           resp(data);
          } else {
            this._isLoggedIn = false;
            reject(res.response);
          }
        });
  
      }); 
    }

    isLogged(iduser, idpersona){
      this._isLoggedIn = true;
      sessionStorage.setItem( 'isLogin', "true");
      sessionStorage.setItem( 'iduser', iduser);
      this.idUsuario = iduser;
      this.idpersona = idpersona;
    }
   

  }
  