import { Injectable } from '@angular/core';
import { ProcessWebServiceService } from './process-web-service.service';
import { Constants } from '../objects/constans';

@Injectable({
  providedIn: 'root'
})
export class NavigateService {

  section: string;
  constructor() { }


  setSection(section: string){
    this.section = section;
  }

  getSection(){
    //console.log(this.section);    
    return this.section;
  }
  /* navigateTo(section: string){
    switch (section) {
      case "about"{
        
      }
    }
  } */
}
