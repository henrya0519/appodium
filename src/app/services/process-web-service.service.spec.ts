import { TestBed } from '@angular/core/testing';

import { ProcessWebServiceService } from './process-web-service.service';

describe('ProcessWebServiceService', () => {
  let service: ProcessWebServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcessWebServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
