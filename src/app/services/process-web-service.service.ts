import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../objects/constans';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { RequestProcess } from '../objects/request';
import { catchError } from 'rxjs/operators';
import { throwError as observableTrowError }  from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProcessWebServiceService {
  constantes = new Constants();

  constructor(private http: HttpClient) { }

  errorHandler(error: HttpErrorResponse){
    return observableTrowError(error);
}

  execProcess(data: any): Observable<any> {
    
    //const jsonPeticion =  JSON.stringify(peticion);
    //console.log(data)
    /* let dataCon: any = {
            
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 5,
        consulta: {
          ip: data.ip,
          cuenta:  data.cuenta,
          clave: data.clave,
        }
    } */

    const jsonPeticion = JSON.stringify(data);   
    console.log("{\"data\":" + JSON.stringify(jsonPeticion) + ' }')
    
    // Establecemos cabeceras
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.constantes.getEndpoint(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
    // .pipe(map(  result => result ));
  }

  recoveryPass(data: any): Observable<any> {
    const jsonPeticion = JSON.stringify(data);    
    // Establecemos cabeceras
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.constantes.getRecoveryPass(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
  }

  otp(data: any): Observable<any> {
    const jsonPeticion = JSON.stringify(data);    
    // Establecemos cabeceras
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.constantes.getOTP(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
  }

  resultTriage(data: any): Observable<any> {
    const jsonPeticion = JSON.stringify(data);    
    // Establecemos cabeceras
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.constantes.getResultTriage(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
  }

  medicalSoft(data: any): Observable<any>{
    const jsonPeticion = data;    
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.constantes.getEndpointMedical(), jsonPeticion, { headers: headers, responseType: 'text' }, )
    .pipe( catchError(this.errorHandler));
  }

  sendMail(data: any){
    const jsonPeticion = JSON.stringify(data);
    // Establecemos cabeceras
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.constantes.getContact(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
  } 

  rescheduleAppointment(data:any){
    const jsonPeticion = JSON.stringify(data);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    console.log( "{\"data\":" + JSON.stringify(jsonPeticion) + ' }');
    return this.http.post<any>(this.constantes.getRescheduleAppointment(), "{\"data\":" + JSON.stringify(jsonPeticion) + ' }', { headers: headers });
  }

}
