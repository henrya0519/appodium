import { Injectable } from '@angular/core';
import { ProcessWebServiceService } from './process-web-service.service';
import { Constants } from '../objects/constans';


@Injectable({
    providedIn: 'root'
  })

  export class TriageService {
  
    headerOptions: any = null;
    _isLoggedIn: boolean = false;
    userLogged: string;
    userPass: string;
    cuenta:string;
    clave:string;
    idUsuario : number;

    constantes = new Constants();
    triageLigth: any;
    constructor(
     private _processWebService:  ProcessWebServiceService,
    ) {                
    }
  
    loadTriageL() {
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 1,
        consulta: {
          schema: "negocio",
          tabla: "preguntastriagegratis_v",
          campo: "*"
        }
      }
      this._processWebService.execProcess(data)
        .subscribe(res => {
          if (res != null) {
            this.triageLigth  = JSON.parse(res.response)         
          }
        })
    }

    getTriageL(){
      return this.triageLigth;
    }
  }
  