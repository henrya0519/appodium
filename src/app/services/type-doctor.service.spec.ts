import { TestBed } from '@angular/core/testing';

import { TypeDoctorService } from './type-doctor.service';

describe('TypeDoctorService', () => {
  let service: TypeDoctorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeDoctorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
