import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TypeDoctorService {

  doctor: string;
  constructor() { }

  setDoctor(doctor: string){
    this.doctor = doctor;
  }

  getDoctor(){
    return this.doctor;
  }
}
