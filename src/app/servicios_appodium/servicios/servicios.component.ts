import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavigateService } from 'src/app/services/navigate.service';
import {Constants} from 'src/app/objects/constans';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})
export class ServiciosComponent implements OnInit {

  constants = new Constants();
  
  mobile= false;
  serviceType: any;
  service: {id: number}
  boy=false;
  adulto=false;
  adultoMayor=false;
  iconButton = false;
  habeas = this.constants.habeasData;

  constructor(private rutaActiva: ActivatedRoute, 
    private toSection: NavigateService) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.service = {
      id: this.rutaActiva.  snapshot.params.id
    };

    this.rutaActiva.params.subscribe((params: Params) => {
      this.service.id = params.id;
    });

    this.viewService(this.service.id);
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    };

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };
  }

  viewService(serviceType){
    
    if(serviceType == 1){
      this.boy = true;
      this.adulto = this.adultoMayor = false;
    } else if (serviceType == 2){
      this.adulto = true;
      this.boy = this.adultoMayor = false;
    } else {
      this.adultoMayor= true;
      this.adulto = this.boy = false;
    }   
  }

  navigateToSection(){
    this.toSection.setSection("servicios");
  }

  navigateToNext(){
    this.toSection.setSection("howWork");
  }


}
