export interface ICarouselItem {
    id: number;
    title?: {
        first: string;
        secondMobile: string;
        second: string;
        third?: string;
        fourth?: string;
        fith?: string;
        sixth?: string;
        seventh?: string;
        eigth?: string;
        ninth?: string;
        tenth?: string;
        eleventh?: string;
        twelfth?: string;
        thirteenth?: string;
        fifteenth?: string;
    };
    subtitle?: string;
    link?: string;
    image: string;
    order?: number;
    marginLeft?: number;
}
