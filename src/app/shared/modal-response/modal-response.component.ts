import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-response',
  templateUrl: './modal-response.component.html',
  styleUrls: ['./modal-response.component.css']
})
export class ModalResponseComponent implements OnInit {

  colorFondo: any;
  description: string;

  constructor( private dialogRef: MatDialogRef<ModalResponseComponent>, @Inject(MAT_DIALOG_DATA) data ) { 
    this.description = data;

  }

  ngOnInit(): void {

  }
 
  
}
