import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Modal } from 'bootstrap';


import { ThrowStmt } from '@angular/compiler';
import { Constants } from 'src/app/objects/constans';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { LoginService } from 'src/app/services/login-service.service';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { sha512 } from 'js-sha512';
import { Router } from '@angular/router';


@Component({
  selector: 'app-test-mobile',
  templateUrl: './test-mobile.component.html',
  styleUrls: ['./test-mobile.component.css']
})
export class TestMobileComponent implements OnInit {

  constantes = new Constants();
  iduser: number;
  mobile = false;
  triageLight1 = true;
  triageLight2 = false;
  form1 = false;
  form2 = false;
  form3 = false;
  form4 = false;
  form5 = false;
  form6 = false;
  form7 = false;
  form8 = false;
  tl1: number;
  tl2: number;
  tl3: number;
  tl4: number;
  tl5: number;
  sumatoria: number;
  titleModal: string = '';
  tittleMessage: string;
  messageResult: string = '';
  complete = false;
  headerModal: number = 0;
  userLogged: String;
  pago = false;
  p1: number;
  p2: number;
  p3: number;
  p4: number;
  p5: number;
  p6: number;
  p7: number;
  p8: number;
  p9: number;
  p10: number;
  p11: number;
  p12: number;
  p13: number;
  p14: number;
  p15: number;
  p16: number;
  p17: number;
  p18: number;
  p19: number;
  p20: number;
  completeTriageFull = false;
  puntaje: number;
  triagePago: any;
  messageResultNoAuth = '';

  headerT_light = '';
  t_full_headers: any;
  toLogin = false;
  toRegister = false;

  tl_prg: any;
  tf_prg_sctn_1: any;
  tf_prg_sctn_2: any;
  tf_prg_sctn_3: any;
  tf_prg_sctn_4: any;
  tf_prg_sctn_5: any;
  noAuth=false;
  steps= false;
  step2 = false;  
  messageResultStep2 = 'Finalizaste tu proceso de reconocimiento y clasificación de síntomas, significa que estás listo para iniciar tú appodium. ';
  
  answerTriage = [
    '',
    '',
    '',
  ];

  
  LoginForm: FormGroup;
  userObject: any = {
    user: '',
    upass: ''
  };


  //@ViewChild('myModal1') myModalcloseAddExpenseModal1: ElementRef;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddModalPayent') closeAddModalPayent: ElementRef;
  @ViewChild('closeAddAuthModal') closeAddAuthModal: ElementRef;
  @ViewChild('closeAddModalSelectAuth') closeAddModalSelectAuth: ElementRef; arrageneral


  constructor(
    private processWebService: ProcessWebServiceService,
    private serviceLogin: LoginService,
    private _formBuilder:FormBuilder, 
    private _router: Router,
  ) {
    this.loadTriageL();    
  }

  ngOnInit(): void {
    window.scroll(0,0);

    this.LoginForm = this._formBuilder.group({
      email: new FormControl('',[Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/) ],),
      password: new FormControl('',[Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$")]),
    });
  }

  get correoNoValido() {
    return this.LoginForm.get('email').invalid && this.LoginForm.get('email').touched;
  }
  get passNoValido() {
    return this.LoginForm.get('password').invalid && this.LoginForm.get('password').touched;
  }


  loadTriageL() {

    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "negocio",
        tabla: "preguntastriagegratis_v",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = JSON.parse(res.response)
          this.headerT_light = data[0].seccion
          this.tl_prg = new Array();
          for (let i = 0; i < data[0].preguntas.length; i++) {
            this.tl_prg[i] = data[0].preguntas[i];
          }
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al cargar el Triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      })
  }

  navigateToLogin(){
    this.closeAddExpenseModal.nativeElement.click();
    this._router.navigate(['/register']);  
  }

  navigateToTriagefull(){
    this.closeAddExpenseModal.nativeElement.click();
    this._router.navigate(['/servicios-appodium']);  
  }

  setTypeAuth(typeAuth:string){
    
    switch (typeAuth) {
      case 'login':
        this.toLogin = true;
        this.toRegister = false;
        this.closeAddExpenseModal.nativeElement.click();
        this.modalLogin();
        break;
      case 'register':
        this.toLogin = false;
        this.toRegister = true;
        this.closeAddExpenseModal.nativeElement.click();
        this.modalLogin();
        break;
    }

  }

  login(){
    if(!this.correoNoValido){

      this.userObject.user = this.LoginForm.get('email').value;
      this.userObject.upass = sha512(this.LoginForm.get('password').value);
      
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 5,
        consulta: {
          ip: "192.168.3.127",
          cuenta: this.userObject.user,
          clave: this.userObject.upass,
        }
      }
      this.processWebService.execProcess(data)
        .subscribe( res => {
          if( res.idresponse == 1){
            this.closeAddAuthModal.nativeElement.click();
            this.closeAddModalSelectAuth.nativeElement.click();
            this.serviceLogin.isLogged(JSON.parse(res.response).idusuario, JSON.parse(res.response).idpersona);
            
            const element = document.getElementById('modalPayment') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show(); 
          }else{
            this.closeAddAuthModal.nativeElement.click();
            sessionStorage.setItem( 'isLogin', "false");
            this.messageResult = JSON.parse(res.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          }
            
        })
    }
  }

  register() {
    this.userObject.user = this.LoginForm.get('email').value;
    this.userObject.upass = sha512(this.LoginForm.get('password').value);

    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 4,
      consulta: {
        ip: "192.168.3.127",
        correo: this.userObject.user,
        clave: this.userObject.upass,
      }
    }    
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res.idresponse === 1) {
          this.closeAddAuthModal.nativeElement.click();
          this.closeAddModalSelectAuth.nativeElement.click();
          this.serviceLogin.isLogged(JSON.parse(res.response).idusuario, JSON.parse(res.response).idpersona);
          const element = document.getElementById('modalPayment') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show(); 
        }else{
          this.closeAddAuthModal.nativeElement.click();
          sessionStorage.setItem( 'isLogin', "false");
          this.messageResult = JSON.parse(res.message).nombre;
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();   
        }
      })
  }

 
  changeForm(form: number) {

    switch (form) {
      case 1:
        this.triageLight1 = true;
        this.triageLight2 = false;
        this.form1 = false;
        this.form2 = false;
        this.form3 = false;
        this.form4 = false;
        this.form5 = false;
        this.form6 = false;
        break;
      case 2:
        this.triageLight2 = true;
        this.triageLight1 = this.form1 = this.form2 = this.form3 = this.form4 = this.form5 = this.form6 = false;
        break;
      case 3: //begin triage full
        this.form3 = true;
        this.form1 = this.form2 = this.triageLight1 = this.triageLight2 = false;
        break;
      default:

    }

  }

  resultTriage() {
 
    if (this.tl1 != undefined && this.tl2 != undefined && this.tl3 != undefined && this.tl4 != undefined && this.tl5 != undefined) {
      this.complete = true;
      this.sumatoria = (+this.tl1) + (+this.tl2) + (+this.tl3) + (+this.tl4) + (+this.tl5);

      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 9,
        consulta: {
          ip: "192.168.3.127",
          data: [
            {
              idseccion: 1,
              pregunta: [{
                idpregunta: this.tl_prg[0].idpreguntagratis,
                idrespuesta: this.tl1
              },
              {
                idpregunta: this.tl_prg[1].idpreguntagratis,
                idrespuesta: this.tl2
              },
              {
                idpregunta: this.tl_prg[2].idpreguntagratis,
                idrespuesta: this.tl3
              },
              {
                idpregunta: this.tl_prg[3].idpreguntagratis,
                idrespuesta: this.tl4
              },
              {
                idpregunta: this.tl_prg[4].idpreguntagratis,
                idrespuesta: this.tl5
              }
              ]
            }]
        }
      }
      this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = res;
          if (data.message == "OK") {
            let respuesta = JSON.parse(data.response);
            this.titleModal = respuesta.nombre;
            this.puntaje = respuesta.puntaje;
            this.messageResult = respuesta.itemsresultado[0].nombre;
            this.changeForm(3);
           
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = JSON.parse(data.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al evaluar el triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      })
    } else {
      this.complete = false;
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe responder todas las preguntas';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }
    
  }

  resetQuestion() {
    this.p1 = this.p2 = this.p3 = this.p4 = this.p5 = this.p6 = this.p7 = this.p8 = this.p9 = this.p10 = 0;
    this.p11 = this.p12 = this.p13 = this.p14 = this.p15 = this.p16 = this.p17 = this.p18 = this.p19 = this.p20 = 0;
  }

  validateLogin() {
    this.sumatoria = this.tl1 = this.tl2 = this.tl3 = this.tl4 = this.tl5 = 0;

    this.complete = false;
    this.userLogged = sessionStorage.getItem('isLogin');
    this.iduser = this.serviceLogin.idUsuario;    

    if(this.userLogged == null || this.userLogged == undefined || this.userLogged == 'false' ){
      this.noAuth = true;
      this.titleModal = 'Informacion';
      this.messageResultNoAuth = 'Debes ingresar para continuar con el proceso.';
    } 
   
  
  }

  validatePayment() {
    this.closeAddModalPayent.nativeElement.click();
    if (!this.pago) {
      this.changeForm(3);
    }
  }

  validatePaymentOfert() {
    this.closeAddModalPayent.nativeElement.click();
    if (!this.pago) {
    }
  }

  resultTriageFull() {

    if (this.p17 != 0 && this.p18 != 0 && this.p19 != 0 && this.p20 != 0) {
      this.complete = false;
      this.completeTriageFull = true;

      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 8,
        consulta: {
          ip: "192.168.3.127",
          idusuario: this.iduser,
          data: [
            {
              idseccion: this.triagePago[0].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[0][0].idpregunta,
                idrespuesta: this.p1
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][1].idpregunta,
                idrespuesta: this.p2
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][2].idpregunta,
                idrespuesta: this.p3
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][3].idpregunta,
                idrespuesta: this.p4
              }]
            },
            {
              idseccion: this.triagePago[1].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[1][0].idpregunta,
                idrespuesta: this.p5
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][1].idpregunta,
                idrespuesta: this.p6
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][2].idpregunta,
                idrespuesta: this.p7
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][3].idpregunta,
                idrespuesta: this.p8
              }]
            },
            {
              idseccion: this.triagePago[2].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[2][0].idpregunta,
                idrespuesta: this.p9
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][1].idpregunta,
                idrespuesta: this.p10
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][2].idpregunta,
                idrespuesta: this.p11
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][3].idpregunta,
                idrespuesta: this.p12
              }]
            },
            {
              idseccion: this.triagePago[3].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[3][0].idpregunta,
                idrespuesta: this.p13
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][1].idpregunta,
                idrespuesta: this.p14
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][2].idpregunta,
                idrespuesta: this.p15
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][3].idpregunta,
                idrespuesta: this.p16
              }]
            },
            {
              idseccion: this.triagePago[4].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[4][0].idpregunta,
                idrespuesta: this.p17
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][1].idpregunta,
                idrespuesta: this.p18
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][2].idpregunta,
                idrespuesta: this.p19
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][3].idpregunta,
                idrespuesta: this.p20
              }]
            }]
        }
      }
      this.processWebService.execProcess(data)
        .subscribe(res => {
          if (res != null) {
            let data = res;
            if (data.message == "OK") {
              let respuesta = JSON.parse(data.response);
              this.titleModal = respuesta.accion;
              this.puntaje = respuesta.puntaje;
              this.messageResult = respuesta[0].itemsresultado[0].item;
              this.validateLogin();
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              this.resetQuestion();
              myModal.show();
            } else {
              this.titleModal = 'Informacion';
              this.messageResult = JSON.parse(data.message).nombre;
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              myModal.show();
            }
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = 'Error al evaluar el triage';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        })
    
    } else {
      this.complete = false;
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe responder todas las preguntas';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }
  }

  modalLogin(){
    this.titleModal = 'Bienvenido a Appodium';
    const element = document.getElementById('authModal') as HTMLElement;
    const modalError = new Modal(element);
    modalError.show();
  }

  step2Modal(){
    this.step2 = true;
    this.steps = true;    
  }
}
