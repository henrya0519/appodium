import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalResponseComponent } from 'src/app/shared/modal-response/modal-response.component';

import { Modal } from 'bootstrap';
import { ThrowStmt } from '@angular/compiler';
import { Constants } from 'src/app/objects/constans';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { LoginService } from 'src/app/services/login-service.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constantes = new Constants();
  iduser: number;
  mobile = false;
  triageLight1 = true;
  triageLight2 = false;
  form1 = false;
  form2 = false;
  form3 = false;
  form4 = false;
  form5 = false;
  form6 = false;
  form7 = false;
  form8 = false;
  tl1: number;
  tl2: number;
  tl3: number;
  tl4: number;
  tl5: number;
  sumatoria: number;
  titleModal: string = '';
  tittleMessage: string;
  messageResult: string = '';
  complete = false;
  headerModal: number = 0;
  userLogged: String;
  pago = false;
  p1: number;
  p2: number;
  p3: number;
  p4: number;
  p5: number;
  p6: number;
  p7: number;
  p8: number;
  p9: number;
  p10: number;
  p11: number;
  p12: number;
  p13: number;
  p14: number;
  p15: number;
  p16: number;
  p17: number;
  p18: number;
  p19: number;
  p20: number;
  completeTriageFull = false;
  puntaje: number;
  triagePago: any;
  preguntasTriageFree: any;

  headerT_light = '';
  t_full_headers: any;

  tl_prg: any;
  tf_prg_sctn_1: any;
  tf_prg_sctn_2: any;
  tf_prg_sctn_3: any;
  tf_prg_sctn_4: any;
  tf_prg_sctn_5: any;
  steps = false;

  answerTriage = [
    '',
    '',
    '',
  ];

  //@ViewChild('myModal1') myModalcloseAddExpenseModal1: ElementRef;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddModalPayent') closeAddModalPayent: ElementRef;


  constructor(private dialog: MatDialog, private processWebService: ProcessWebServiceService, private serviceLogin: LoginService) {
    this.loadTriageL();
    this.loadTriageFull();
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    }
  }

  loadTriageL() {
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "negocio",
        tabla: "preguntastriagegratis_v",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = JSON.parse(res.response)          
          this.headerT_light = data[0].seccion
          this.tl_prg = new Array();
          for (let i = 0; i < data[0].preguntas.length; i++) {
            this.tl_prg[i] = data[0].preguntas[i];
          }
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al cargar el Triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }

      })
  }

  loadTriageFull() {
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "negocio",
        tabla: "preguntastriage_v",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = JSON.parse(res.response)
          this.t_full_headers = new Array();
          this.tf_prg_sctn_1 = new Array();
          this.triagePago = data;
          for (let i = 0; i < data.length; i++) {
            this.t_full_headers[i] = data[i].seccion
          }
          data.forEach(el => {
            this.tf_prg_sctn_1.push(el.preguntas)
          })
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al cargar el Triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }

      })
  }

  changeForm(form: number) {
    switch (form) {
      case 1:
        this.triageLight1 = true;
        this.triageLight2 = false;
        this.form1 = false;
        this.form2 = false;
        this.form3 = false;
        this.form4 = false;
        this.form5 = false;
        this.form6 = false;
        break;
      case 2:
        this.triageLight2 = true;
        this.triageLight1 = this.form1 = this.form2 = this.form3 = this.form4 = this.form5 = this.form6 = false;
        break;
      case 3: //begin triage full
        this.form3 = true;
        this.form1 = this.form2 = this.form4 = this.form5 = this.form6 = this.triageLight1 = this.triageLight2 = false;
        break;
      case 4:
        if (this.p1 != 0 && this.p2 != 0 && this.p3 != 0 && this.p4 != 0) {
          this.form4 = true;
          this.form1 = this.form3 = this.form2 = this.form5 = this.form6 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Debe responder todas las preguntas';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }        
        break;
      case 5:
        if (this.p5 != 0 && this.p6 != 0 && this.p7 != 0 && this.p8 != 0) {
          this.form5 = true;
          this.form1 = this.form3 = this.form2 = this.form4 = this.form6 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Debe responder todas las preguntas';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
        break;
      case 6:
        if (this.p9 != 0 && this.p10 != 0 && this.p11 != 0 && this.p12 != 0) {
          this.form6 = true;
          this.form1 = this.form3 = this.form2 = this.form4 = this.form5 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Debe responder todas las preguntas';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
        break;

      case 7:
        if (this.p13 != 0 && this.p14 != 0 && this.p15 != 0 && this.p16 != 0) {
          this.form7 = true;
          this.form1 = this.form3 = this.form2 = this.form4 = this.form5 = this.form6 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Debe responder todas las preguntas';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
        break;

      case 8:
        if (this.p17 != 0 && this.p18 != 0 && this.p19 != 0 && this.p20 != 0) {
          this.form8 = true;
          this.form1 = this.form3 = this.form2 = this.form4 = this.form5 = this.form6 = this.form7 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Debe responder todas las preguntas';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
        break;

      default:

    }
  }

  resultTriage() {
    if (this.tl1 != undefined && this.tl2 != undefined && this.tl3 != undefined && this.tl4 != undefined && this.tl5 != undefined) {
      this.complete = true;
      this.sumatoria = (+this.tl1) + (+this.tl2) + (+this.tl3) + (+this.tl4) + (+this.tl5);

      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 9,
        consulta: {
          ip: "192.168.3.127",
          data: [
            {
              idseccion: 1,
              pregunta: [{
                idpregunta: this.tl_prg[0].idpreguntagratis,
                idrespuesta: this.tl1
              },
              {
                idpregunta: this.tl_prg[1].idpreguntagratis,
                idrespuesta: this.tl2
              },
              {
                idpregunta: this.tl_prg[2].idpreguntagratis,
                idrespuesta: this.tl3
              },
              {
                idpregunta: this.tl_prg[3].idpreguntagratis,
                idrespuesta: this.tl4
              },
              {
                idpregunta: this.tl_prg[4].idpreguntagratis,
                idrespuesta: this.tl5
              }
              ]
            }]
        }
      }
      this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = res;
          if (data.message == "OK") {
            let respuesta = JSON.parse(data.response);
            this.titleModal = respuesta.nombre;
            this.puntaje = respuesta.puntaje;
            this.messageResult = respuesta.itemsresultado[0].nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            this.resetQuestion();
            myModal.show();
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = JSON.parse(data.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al evaluar el triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      })
    
    } else {
      this.complete = false;
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe responder todas las preguntas';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }

  }

  validateLogin() {
    this.sumatoria = this.tl1 = this.tl2 = this.tl3 = this.tl4 = this.tl5 = 0;
    this.closeAddExpenseModal.nativeElement.click();
    this.complete = false;
    this.userLogged = sessionStorage.getItem('isLogin');
    this.iduser = this.serviceLogin.idUsuario;
    //si el usuario no esta loggeado modal de auth si lo esta modal de pago 
    if (this.userLogged == "true") {
      this.steps = true;
      this.titleModal = 'Informacion';
      this.messageResult = 'Bien!!! finalizaste tu proceso de reconocimiento y clasificación de síntomas, significa que estás listo para iniciar tú appodium, es decir, iniciar el apoyo por un especialista de la psicología. ';      
      const element = document.getElementById('myModal') as HTMLElement;
      const modalError = new Modal(element);
      modalError.show();

    } else {
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe ingresar para continuar con el siguiente Triage.';
      const element = document.getElementById('myModal') as HTMLElement;
      const modalError = new Modal(element);
      modalError.show();
    }
  }

  validatePayment() {
    this.closeAddModalPayent.nativeElement.click();
    if (!this.pago) {
      this.changeForm(3);
    }
  }

  validatePaymentOfert() {
    this.closeAddModalPayent.nativeElement.click();
    if (!this.pago) {

    }
  }

  resultTriageFull() {

    if (this.p17 != 0 && this.p18 != 0 && this.p19 != 0 && this.p20 != 0) {
      this.complete = false;
      this.completeTriageFull = true;

      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 8,
        consulta: {
          ip: "192.168.3.127",
          idusuario: this.iduser,
          data: [
            {
              idseccion: this.triagePago[0].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[0][0].idpregunta,
                idrespuesta: this.p1
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][1].idpregunta,
                idrespuesta: this.p2
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][2].idpregunta,
                idrespuesta: this.p3
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][3].idpregunta,
                idrespuesta: this.p4
              }]
            },
            {
              idseccion: this.triagePago[1].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[1][0].idpregunta,
                idrespuesta: this.p5
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][1].idpregunta,
                idrespuesta: this.p6
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][2].idpregunta,
                idrespuesta: this.p7
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][3].idpregunta,
                idrespuesta: this.p8
              }]
            },
            {
              idseccion: this.triagePago[2].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[2][0].idpregunta,
                idrespuesta: this.p9
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][1].idpregunta,
                idrespuesta: this.p10
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][2].idpregunta,
                idrespuesta: this.p11
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][3].idpregunta,
                idrespuesta: this.p12
              }]
            },
            {
              idseccion: this.triagePago[3].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[3][0].idpregunta,
                idrespuesta: this.p13
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][1].idpregunta,
                idrespuesta: this.p14
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][2].idpregunta,
                idrespuesta: this.p15
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][3].idpregunta,
                idrespuesta: this.p16
              }]
            },
            {
              idseccion: this.triagePago[4].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[4][0].idpregunta,
                idrespuesta: this.p17
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][1].idpregunta,
                idrespuesta: this.p18
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][2].idpregunta,
                idrespuesta: this.p19
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][3].idpregunta,
                idrespuesta: this.p20
              }]
            }]
        }
      }
      this.processWebService.execProcess(data)
        .subscribe(res => {
          if (res != null) {
            let data = res;
            if (data.message == "OK") {
              let respuesta = JSON.parse(data.response);
              this.titleModal = respuesta.accion;
              this.puntaje = respuesta.puntaje;
              this.messageResult = respuesta[0].itemsresultado[0].item;
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              this.resetQuestion();
              myModal.show();
            } else {
              this.titleModal = 'Informacion';
              this.messageResult = JSON.parse(data.message).nombre;
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              myModal.show();
            }
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = 'Error al evaluar el triage';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        })
 
    } else {
      this.complete = false;
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe responder todas las preguntas';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }

  }

  resetQuestion() {
    this.p1 = this.p2 = this.p3 = this.p4 = this.p5 = this.p6 = this.p7 = this.p8 = this.p9 = this.p10 = 0;
    this.p11 = this.p12 = this.p13 = this.p14 = this.p15 = this.p16 = this.p17 = this.p18 = this.p19 = this.p20 = 0;
  }

  
}




