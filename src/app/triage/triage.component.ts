import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RegisterModalComponent } from '../auth/register-modal/register-modal.component';


@Component({
  selector: 'app-triage',
  templateUrl: './triage.component.html',
  styleUrls: ['./triage.component.css']
})
export class TriageComponent implements OnInit {

  mobile= false;

  constructor(public dialog: MatDialog, ) { 
  }

  ngOnInit(): void {
    window.scroll(0,0);

    if (window.screen.width <= 992) { // 768px portrait
      this.mobile = true;
    }
  }

  openModal(){
    const dialogRef = this.dialog.open(RegisterModalComponent, {
      height: '520px', 
    }
      );
    dialogRef.afterClosed().subscribe( res => {
    })
  }
}
