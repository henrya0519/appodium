import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriageFullComponent } from './triage-full.component';

describe('TriageFullComponent', () => {
  let component: TriageFullComponent;
  let fixture: ComponentFixture<TriageFullComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriageFullComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriageFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
