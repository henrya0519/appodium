import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Modal } from 'bootstrap';

import { Constants } from 'src/app/objects/constans';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { LoginService } from 'src/app/services/login-service.service';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { sha512 } from 'js-sha512';
import { Router } from '@angular/router';
import { NgbSlideEvent, NgbSlideEventSource, NgbTimepickerConfig, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-triage-full',
  templateUrl: './triage.component.html',
  styleUrls: ['./triage.component.css']
})
export class TriageFormComponent implements OnInit {

  dataUserForm: FormGroup;   
  appointmentForm: FormGroup; 
  idUsuario = '';
  constantes = new Constants();
  iduser: number;

  mobile = false;
  triageLight1 = true;
  triageLight2 = false;
  form1 = false;
  form2 = false;
  form3 = false;
  form4 = false;
  form5 = false;
  form6 = false;
  form7 = false;
  form8 = false;
  tl1: number;
  tl2: number;
  tl3: number;
  tl4: number;
  tl5: number;
  sumatoria: number;
  titleModal: string = '';
  tittleMessage: string;
  messageResult: string = '';
  complete = false;
  headerModal: number = 0;
  userLogged: String;
  pago = false;
  p1: number;
  p2: number;
  p3: number;
  p4: number;
  p5: number;
  p6: number;
  p7: number;
  p8: number;
  p9: number;
  p10: number;
  p11: number;
  p12: number;
  p13: number;
  p14: number;
  p15: number;
  p16: number;
  p17: number;
  p18: number;
  p19: number;
  p20: number;
  completeTriageFull = false;
  puntaje: number;
  triagePago: any;
  messageResultNoAuth = '';
  resultTriage = false;
  puntajeTriage = '';
  datauser = false;
  payment = false;

  headerT_light = '';
  t_full_headers: any;
  toLogin = false;
  toRegister = false;

  tl_prg: any;
  tf_prg_sctn_1: any;
  tf_prg_sctn_2: any;
  tf_prg_sctn_3: any;
  tf_prg_sctn_4: any;
  tf_prg_sctn_5: any;
  noAuth=false;
  steps= false;
  step2 = false;
  messageResultStep2 = 'Bien!!! finalizaste tu proceso de reconocimiento y clasificación de síntomas, significa que estás listo para iniciar tú appodium, es decir, iniciar el apoyo por un especialista de la psicología. ';      
  documentsType: any;
  escolaridad: any;
  ocupaciones: any;
  idpersona: number;
  ciudades: any;
  user_email: any;
  typeDoc:string;
  appoinment = false;
  timeAppointment: any;
  dt = new Date();
  dia = this.dt.getUTCDate();
  mes =  (this.dt.getUTCMonth() <9 )? ('0' + (this.dt.getUTCMonth()+ 1)) : ( this.dt.getUTCMonth() <9 + 1);
  ano = this.dt.getUTCFullYear();
  appointmenSuccess = false;
  iconButton = false;

  answerTriage = [
    '',
    '',
    '',
  ];

  
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddModalPayment') closeAddModalPayment: ElementRef;
  @ViewChild('closeAddAuthModal') closeAddAuthModal: ElementRef;
  @ViewChild('closeAddModalSelectAuth') closeAddModalSelectAuth: ElementRef;
  @ViewChild('closeAddModalAppointment') closeAddModalAppointment: ElementRef;  
  @ViewChild('closeAddPayModal') closeModalPay: ElementRef;

  constructor(
    private processWebService: ProcessWebServiceService,
    private serviceLogin: LoginService,
    private _formBuilder:FormBuilder, 
    private _router: Router,
  ) { 
    this.idUsuario = sessionStorage.getItem('iduser');
    this.validateDataUser();
    this.consultTriageResult(); 
    this.consultDocuments();
    this.consultEscolaridad();
    this.consultOcupaciones();
    this.consultaciudades();      
  }

  ngOnInit(): void {    
    this.toTop();

    this.dataUserForm = this._formBuilder.group({
      idtipodocumento: new FormControl('Tipo de Documento', [Validators.required]),
      documento: new FormControl('', [Validators.required]),
      nombre: new FormControl('', [Validators.required]),
      apellido: new FormControl('', [Validators.required]),
      telefono: new FormControl('', [Validators.required]),
      fechanacimiento: new FormControl('', [Validators.required]),
      authorizationData: new FormControl('', Validators.required)
    });

    this.appointmentForm = this._formBuilder.group({        
      documentoCliente: new FormControl('', [Validators.required]),
      fechaCita: new FormControl('', [Validators.required]),
      horaCita: new FormControl('', Validators.required),
      nombreCliente: new FormControl('', [Validators.required]),
      telefonoCliente: new FormControl('', [Validators.required]),
      correoCliente: new FormControl('', [Validators.required]),
      motivoConsulta: new FormControl(''),
      tipoConsulta: new FormControl('-1', Validators.required)   
    });

    if (window.screen.width <= 768){
      this.iconButton = true;
    } else {
      this.iconButton = false;
    };
    this.validateDataUser();
  }

  toTop(){
    window.scroll(0,0);
  }

  loadTriageFull() {
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "negocio",
        tabla: "preguntastriage_v",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe(res => {
        if (res != null) {
          let data = JSON.parse(res.response)
          this.t_full_headers = new Array();
          this.tf_prg_sctn_1 = new Array();
          this.triagePago = data;

          for (let i = 0; i < data.length; i++) {
            this.t_full_headers[i] = data[i].seccion
          }
          data.forEach(el => {
            this.tf_prg_sctn_1.push(el.preguntas)
          })
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'Error al cargar el Triage';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }

      })
  }

  /* Data formulario */  
  consultDocuments(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "tipodocumentos",
        campo: "*",
        condicion: "cliente=1 and estado=1",
        orden:"idtipodocumento"
      }
    }  
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.documentsType = JSON.parse(res.response);
      })
  }

  consultEscolaridad(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "escolaridades",
        campo: "*"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.escolaridad = JSON.parse(res.response);
      })
  }

  consultOcupaciones(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "admin",
        tabla: "ocupaciones",
        campo: "*",
        orden: "nombre"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.ocupaciones = JSON.parse(res.response);
      })
  }

  consultaciudades(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "gis",
        tabla: "ciudades",
        campo: "*",
        orden: "nombre"
      }
    }
    this.processWebService.execProcess(data)
      .subscribe( res => {
        this.ciudades = JSON.parse(res.response);
      })
  }

  changeForm(form: number) {

    switch (form) {
      case 1: //begin triage full
        this.form1 = true;
        this.form2 = false;
        this.form3 = false;
        this.form4 = false;
        this.form5 = false;
        this.form6 = false;
        break;
      case 2:
        if (this.p1 != undefined && this.p2 != undefined && this.p3 != undefined && this.p4 != undefined) {
          this.form2 = true;
          this.form1 = this.form3 = this.form5 = this.form4 = this.form6 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'debe responder todas las preguntas';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }        
        break;
      case 3:
        if (this.p5 != undefined && this.p6 != undefined && this.p7 != undefined && this.p8 != undefined) {
          this.form3 = true;
          this.form1 = this.form2 = this.form4 = this.form5 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'debe responder todas las preguntas';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }        
        break;
      case 4:
        if (this.p9 != undefined && this.p10 != undefined && this.p11 != undefined && this.p12 != undefined) {         
          this.form4 = true;
          this.form1 = this.form3 = this.form2 = this.form5 = this.form6 = false;
        } else {
          this.titleModal = 'Informacion';
          this.messageResult = 'debe responder todas las preguntas';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
        break;
      case 5:
          if (this.p13 != undefined && this.p14 != undefined && this.p15 != undefined && this.p16 != undefined) {
            this.form5 = true;
            this.form1 = this.form3 = this.form2 = this.form4 = false;
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = 'debe responder todas las preguntas';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        break;

      case 6:
       
        break;      

      default:

    }

  }

  resultTriageFull() {    
    if (this.p17 != 0 && this.p18 != 0 && this.p19 != 0 && this.p20 != 0) {

      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 8,
        consulta: {
          ip: "192.168.3.127",
          idusuario: this.idUsuario,
          data: [
            {
              idseccion: this.triagePago[0].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[0][0].idpregunta,
                idrespuesta: this.p1
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][1].idpregunta,
                idrespuesta: this.p2
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][2].idpregunta,
                idrespuesta: this.p3
              },
              {
                idpregunta: this.tf_prg_sctn_1[0][3].idpregunta,
                idrespuesta: this.p4
              }]
            },
            {
              idseccion: this.triagePago[1].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[1][0].idpregunta,
                idrespuesta: this.p5
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][1].idpregunta,
                idrespuesta: this.p6
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][2].idpregunta,
                idrespuesta: this.p7
              },
              {
                idpregunta: this.tf_prg_sctn_1[1][3].idpregunta,
                idrespuesta: this.p8
              }]
            },
            {
              idseccion: this.triagePago[2].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[2][0].idpregunta,
                idrespuesta: this.p9
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][1].idpregunta,
                idrespuesta: this.p10
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][2].idpregunta,
                idrespuesta: this.p11
              },
              {
                idpregunta: this.tf_prg_sctn_1[2][3].idpregunta,
                idrespuesta: this.p12
              }]
            },
            {
              idseccion: this.triagePago[3].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[3][0].idpregunta,
                idrespuesta: this.p13
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][1].idpregunta,
                idrespuesta: this.p14
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][2].idpregunta,
                idrespuesta: this.p15
              },
              {
                idpregunta: this.tf_prg_sctn_1[3][3].idpregunta,
                idrespuesta: this.p16
              }]
            },
            {
              idseccion: this.triagePago[4].idseccion,
              pregunta: [{
                idpregunta: this.tf_prg_sctn_1[4][0].idpregunta,
                idrespuesta: this.p17
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][1].idpregunta,
                idrespuesta: this.p18
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][2].idpregunta,
                idrespuesta: this.p19
              },
              {
                idpregunta: this.tf_prg_sctn_1[4][3].idpregunta,
                idrespuesta: this.p20
              }]
            }]
        }
      }
      this.processWebService.execProcess(data)
        .subscribe(res => {
          if (res != null) {
            let data = res;
            if (data.message == "OK") {
              let respuesta = JSON.parse(data.response);           

              this.titleModal = respuesta.accion;
              this.puntaje = respuesta.puntaje;
              this.messageResult = respuesta.itemsresultado[0].item;
              
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              myModal.show();
              this.resetQuestion();
              this.sendResultTriage(0);
              
              /*  let data_ = {
                cuenta: this.constantes.getCue(),
                clave: this.constantes.getClave(),
                proceso: 1,
                consulta: {
                  schema:"transaccional",
                  tabla:"triage_v",
                  campo:"*",                  
                }
              }
              this.processWebService.execProcess(data_)
              .subscribe(res => {
                console.log("Response Triage!!!!!!!!!!", res);
              }) */
            
            } else {
              this.titleModal = 'Informacion';
              this.messageResult = JSON.parse(data.message).nombre;
              const element = document.getElementById('myModal') as HTMLElement;
              const myModal = new Modal(element);
              myModal.show();
            }
          } else {
            this.titleModal = 'Informacion';
            this.messageResult = 'Error al evaluar el triage';
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();
          }
        })     
    } else {
      this.complete = false;
      this.titleModal = 'Informacion';
      this.messageResult = 'Debe responder todas las preguntas';
      const element = document.getElementById('myModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }
    //this.openModal();
  }

  resetQuestion() {
    this.p1 = this.p2 = this.p3 = this.p4 = this.p5 = this.p6 = this.p7 = this.p8 = this.p9 = this.p10 = 0;
    this.p11 = this.p12 = this.p13 = this.p14 = this.p15 = this.p16 = this.p17 = this.p18 = this.p19 = this.p20 = 0;
  }

  /* verificar si se usa en este componente si no eliminar */
  validateLogin() {

    this.complete = false;
    this.userLogged = sessionStorage.getItem('isLogin');
    this.iduser = +sessionStorage.getItem('iduser');

    if(this.userLogged == null || this.userLogged == undefined || this.userLogged == 'false' ){
      this.noAuth = true;
      this.titleModal = 'Informacion';
      this.messageResultNoAuth = 'Debe ingresar para continuar con el proceso.';
    }     
  }

  //valida si el usuario ya está registrado
  //@loadModal -> 0 no abre modal, 1 abre modal pago, 2 abre modal pago para cita
  validateDataUser(loadModal?){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema:"admin",
        tabla:"infocliente_v",
        campo:"*",
        condicion: 'idusuario = ' + this.idUsuario
      }
    }
    console.log(data);
    this.processWebService.execProcess(data)
      .subscribe( (res) => {
        console.log(res)
        if(res.response != null){
          let dataUser = JSON.parse(res.response);
          this.idpersona =  dataUser[0].idpersona;
          console.log(dataUser[0])
          this.user_email =  JSON.parse(res.response)[0].correo
          this.datauser = true;    
        }
        
      })
  }

  //Se asigana tipo de documento para consumo API medical
  setTypeDoc(){      
    if(this.dataUserForm.get('idtipodocumento').value === 1 ){      
      this.typeDoc = 'CC'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 2){ 
      this.typeDoc = 'CE'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 3){
      this.typeDoc = 'PA'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 4){
      this.typeDoc = 'RC'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 5){
      this.typeDoc = 'TI'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 6){
      this.typeDoc = 'MS'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 7){
      this.typeDoc = 'AS'
    }
    if(this.dataUserForm.get('idtipodocumento').value === 8){
      this.typeDoc = 'DNI'
    }

  }

  viewTriageForm(){
//    this.payment = true;
    if(this.datauser){
      this.changeForm(1);
    } else {
      this.titleModal = 'Informacion';
      this.messageResult = 'Por favor registrate o inicia sesión para continuar';
      const element = document.getElementById('authModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();
    }
    
  }

  validatePayment(appointent?: boolean) {
    //deta para registro clientes en MedicalSoft
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 1, //registro cliente
      nombre: this.dataUserForm.get('nombre').value + ' ' + this.dataUserForm.get('apellido').value,
      documento:  this.appointmentForm.get('documentoCliente').value,
      tipoDoc: this.typeDoc,
      fechaNacimiento: this.dataUserForm.get('fechanacimiento').value,
      telefono: this.dataUserForm.get('telefono').value,
      email: this.user_email
    }
    this.processWebService.medicalSoft(data)
      .subscribe(res => {
        if (res != null) {
          this.closeAddModalPayment.nativeElement.click();
          this.titleModal = 'Informacion';
          this.messageResult = 'Pago Realizado con éxito!.';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();

           if (!this.pago) {
              this.payment = true;
              this.changeForm(1);
              this.registerInvoiceTriage();  //registro factura en MedicalSoft
            }
        }        
      })
      /* this.closeAddModalPayment.nativeElement.click();
      this.titleModal = 'Informacion';
      this.messageResult = 'Pago Realizado con éxito!.';
      const element = document.getElementById('authModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();

       if (!this.pago) {
          this.payment = true;
          this.changeForm(1);
          //this.registerInvoiceTriage();  //registro factura en MedicalSoft
        } */

    this.closeAddModalPayment.nativeElement.click(); 
      /* $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();  */  
  }

  validPaymentAppoinment(){
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 1, //registro cliente
      nombre: this.dataUserForm.get('nombre').value + ' ' + this.dataUserForm.get('apellido').value,
      documento:  this.appointmentForm.get('documentoCliente').value,
      tipoDoc: this.typeDoc,
      fechaNacimiento: this.dataUserForm.get('fechanacimiento').value,
      telefono: this.dataUserForm.get('telefono').value,
      email: this.user_email
    }
  /*   this.processWebService.medicalSoft(data)
      .subscribe(res => {
        if (res != null) {
          this.closeAddModalPayment.nativeElement.click();
          this.titleModal = 'Informacion';
          this.messageResult = 'Pago Realizado con éxito!.';
          const element = document.getElementById('authModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();

           if (!this.pago) {
              this.payment = true;
              this.changeForm(1);
              this.registerInvoiceAppoinment();  //registro factura en MedicalSoft
            }
        }
        
      }) */
      this.closeAddModalPayment.nativeElement.click();
      this.titleModal = 'Informacion';
      this.messageResult = 'Pago Realizado con éxito!.';
      const element = document.getElementById('authModal') as HTMLElement;
      const myModal = new Modal(element);
      myModal.show();

       if (!this.pago) {
          this.payment = true;         
        }

    this.closeAddModalPayment.nativeElement.click(); 
      /* $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();  */  
  }

  validatePaymentOfert() {
    this.closeAddModalPayment.nativeElement.click();
    if (!this.pago) {
    }
  }

 navigateToLogin(){
    this.closeAddExpenseModal.nativeElement.click();
    this._router.navigate(['/register']);  
  }

  step2Modal(){
    
    this.step2 = true;
    //this.steps = true;
    this.messageResultStep2 = 'Bien!!! finalizaste tu proceso de reconocimiento y clasificación de síntomas, significa que estás listo para iniciar tú appodium, es decir, iniciar el apoyo por un especialista de la psicología. ';          

    //this.closeAddModalPayment.nativeElement.click();
    this.titleModal = 'Informacion';
    this.messageResult = 'Pago Realizado con éxito!.';
    const element = document.getElementById('myModalEnd') as HTMLElement;
    const myModalEnd = new Modal(element);
    myModalEnd.show();

  }

  consultTriageResult(){
    let data = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema: "transaccional",
        tabla: "triages",
        campo: "*",
        condicion: 'idusuario = '+ this.idUsuario 
      }
    }
    this.processWebService.execProcess(data)
    .subscribe(res => {
      if (res != null) {
        let data = res;
        if(data.response != null){
          this.puntajeTriage =JSON.parse(data.response)[0].puntaje;
          this.resultTriage = true;          
          //this.validateDataUser(0);

        } else{ 
          this. loadTriageFull();
          //this.validateDataUser(1);
        }
        
      } 
     ;
    })
  }

  updateDataUser(){
    let data = {

      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 6,
      consulta: {
        ip: '192.168.0.1',
        idusuario: this.idUsuario,
        idpersona: this.idpersona,
        tipodocumento: this.dataUserForm.get('idtipodocumento').value,
        documento: this.dataUserForm.get('documento').value,
        nombre: this.dataUserForm.get('nombre').value,
        apellido: this.dataUserForm.get('apellido').value,
        telefono: this.dataUserForm.get('telefono').value,
        fechanac: this.dataUserForm.get('fechanacimiento').value,
        /* lugarnac: this.dataUserForm.get('lugarnacimiento').value,
        escolaridad: this.dataUserForm.get('idescolaridad').value,
        ocupacion: this.dataUserForm.get('idocupacion').value,
        direccion: this.dataUserForm.get('direccion').value,
        ciudad: this.dataUserForm.get('idciudad').value, */
      }
    }
    this.processWebService.execProcess(data)
    .subscribe(res => {
      if(res.response != null){                

        this.appointmentForm.get('documentoCliente').setValue(this.dataUserForm.get('idtipodocumento').value);
        this.appointmentForm.get('nombreCliente').setValue(this.dataUserForm.get('nombre').value + ' ' + this.dataUserForm.get('apellido').value);
        this.appointmentForm.get('telefonoCliente').setValue(this.dataUserForm.get('telefono').value);
        this.appointmentForm.get('correoCliente').setValue(this.user_email);            

        this.closeAddExpenseModal.nativeElement.click();
        const element = document.getElementById('modalPayment') as HTMLElement;
        const myModalPay = new Modal(element);
        myModalPay.show();

        //this.validateDataUser();
      } else if (res.message != null) {
        this.messageResult = JSON.parse(res.message).acento;
        const element = document.getElementById('authModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();

        /* error => {
          console.log(error);
          if(error.message != null){
            
          }
        } */

      }
    })
  }
  

setTime(){
  let hora = JSON.stringify(this.appointmentForm.get('horaCita').value?.hour);
  let minutos = JSON.stringify(this.appointmentForm.get('horaCita').value?.minute);
  
  this.timeAppointment = ((hora?.length > 1)? hora : '0'+hora) +':'+ ((minutos?.length > 1 )?  minutos : '0'+minutos) +':00';
}

/* Services MedicalSoft */

registerInvoiceAppoinment(){
  let data = {
    token: this.constantes.getTokenMedical(),
    pass: this.constantes.getPassMedical(),
    tipo: 3, //registro factura
    documentoCliente : '',//this.appointmentForm.get('documentoCliente').value,
    fechaOper: '',
    fechaVen: '',
    descripcion: 'Agendamiento Cita Diagnóstico',
    totalBruto: '50000.00',
    impuestos: '6000.00',
    descuentos: '1000.00',
    totalNeto: '55000.00',
    montoPagado: '55000.00'
  }
  this.processWebService.medicalSoft(data)
    .subscribe( res => {       
      if( res != null){
        this.messageResult = 'Su cita ha sido agendada éxitosamente. Gracias por usar los servicios de Appodium';
        const element = document.getElementById('responseModal') as HTMLElement;
        const myModal = new Modal(element);
        myModal.show();          
        this.appointmenSuccess = true;     
      } 
    },
      /*  error => {
        if(error.status == 200 && error.statusText == "OK"){

        }
      } */
    )
}
/* End Services MedicalSoft */
  
  navigateServices(){
    if(this.appointmenSuccess = true){
      setTimeout(() => {
        this._router.navigate(['/servicios-appodium']);
      }, 1000 );
    }   
  }
  
  registerInvoiceTriage(){
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 3, //registro factura
      documentoCliente : '',//this.appointmentForm.get('documentoCliente').value,
      fechaOper: '',
      fechaVen: '',
      descripcion: 'TRIAGE Full',
      totalBruto: '50000.00',
      impuestos: '6000.00',
      descuentos: '1000.00',
      totalNeto: '55000.00',
      montoPagado: '55000.00'
    }
    this.processWebService.medicalSoft(data)
      .subscribe( res => {       
        
      },
       /*  error => {
          if(error.status == 200 && error.statusText == "OK"){

          }
        } */
      )
  }

  sendResultTriage(value:any){
    let data_ = {
      cuenta: this.constantes.getCue(),
      clave: this.constantes.getClave(),
      proceso: 1,
      consulta: {
        schema:"transaccional",
        tabla:"triage_v",
        campo:"*",
        condicion: 'idusuario = ' + this.idUsuario
      }
    }
    this.processWebService.execProcess(data_)
    .subscribe(res => {            
      let response = JSON.parse(res.response)
      if(res != null){
        let data = {
          user: this.appointmentForm.get('nombreCliente').value,
          dateTriage: response[0].fecha,
          score: response[0].puntaje,
          toDo: response[0].resultado,
          action: response[0].accion,
          itMeans: response[0].itemsresultado[0].item,    
          email: this.user_email,            
        }
        this.processWebService.resultTriage(data)
          .subscribe(res => {
            this.titleModal = 'Informacion';
            if(value == 1){
              this.messageResult = res.message;
              const element = document.getElementById('authModal') as HTMLElement;
              const myModal = new Modal(element);
              myModal.show();
            }                       
            //console.log(res);
          })              
      }            
    })
  }

   //Registra cita en MedicalSoft 
   registerAppoinment(){
    let data = {
      token: this.constantes.getTokenMedical(),
      pass: this.constantes.getPassMedical(),
      tipo: 2, //registra cita
      documentoCliente : this.appointmentForm.get('documentoCliente').value,
      fechaCita: this.appointmentForm.get('fechaCita').value,
      horaCita: this.timeAppointment,
      nombreCliente: this.appointmentForm.get('nombreCliente').value,
      telefonoCliente: '57'+this.appointmentForm.get('telefonoCliente').value,
      correoCliente: this.appointmentForm.get('correoCliente').value,
      motivoConsulta: this.appointmentForm.get('motivoConsulta').value,
      tipoConsulta:this.appointmentForm.get('tipoConsulta').value
    }
    this.processWebService.medicalSoft(data)
      .subscribe( res => {
        this.closeAddModalPayment.nativeElement.click();
        this.closeAddExpenseModal.nativeElement.click();
        this.closeAddModalAppointment.nativeElement.click();
        if(res != null){
          this.registerInvoiceAppoinment();  
          //this.closeAddModalAppointment.nativeElement.click();          
        } else{
          this.messageResult = 'Se presento un error al Agendar su cita. Por favor intente nuevamente.';
          const element = document.getElementById('myModal') as HTMLElement;
          const myModal = new Modal(element);
          myModal.show();
        }
      } )
      this.closeAddModalAppointment.nativeElement.click();      
  }

  navigateTriage(){
    this._router.navigateByUrl('/servicios-appodium');
  }
  
  navigateListDoctors(){
    this._router.navigateByUrl('/agendar-cita');
    
  }
}
