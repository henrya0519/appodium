import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { sha512 } from 'js-sha512';
import { FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import { LoginService } from 'src/app/services/login-service.service';
import { RequestProcess } from 'src/app/objects/request';
import { ProcessWebServiceService } from 'src/app/services/process-web-service.service';
import { Constants } from 'src/app/objects/constans';
import { Router } from '@angular/router';
import { Modal } from 'bootstrap';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent implements OnInit {

  PerfilForm: FormGroup;
  userObject: any = {
    idusuario: '',
    idpersona: '',
    tipoDoc: '',
    nombre: '',
    apellido: '',
    telefono: '',
    fechanac: '',
    lugarnac: '',
    escolaridad: '',
    ocupacion: '',
    direccion: '',
    ciudad: '',
  };
  constantes = new Constants();
  messageResult: string;


  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  constructor(
    private _formBuilder:FormBuilder, 
    private processWebService: ProcessWebServiceService,
    private _router: Router,
    private loginService: LoginService ) { }

  ngOnInit(): void {
    window.scroll(0,0);

    this.PerfilForm = this._formBuilder.group({    
      tipoDocumento: new FormControl(''),
      documento: new FormControl(''),
      nombre: new FormControl(''),
      apellido: new FormControl(''),
      telefono: new FormControl(''),
      fechanac: new FormControl(''),
      lugarnac: new FormControl(''), 
      escolaridad: new FormControl(''),
      ocupacion: new FormControl(''),
      direccion: new FormControl(''),
      ciudad: new FormControl('')      
    });
  }
  
  updateDataUser(){

      this.userObject.tipoDoc = this.PerfilForm.get('tipoDocumento').value;
      this.userObject.documento = this.PerfilForm.get('documento').value;
      this.userObject.nombre = this.PerfilForm.get('nombre').value;
      this.userObject.apellido = this.PerfilForm.get('apellido').value;
      this.userObject.nombre = this.PerfilForm.get('nombre').value;
      this.userObject.telefono = this.PerfilForm.get('telefono').value;
      this.userObject.fechanac = this.PerfilForm.get('fechanac').value;
      this.userObject.lugarnac = this.PerfilForm.get('lugarnac').value;
      this.userObject.escolaridad = this.PerfilForm.get('escolaridad').value;
      this.userObject.ocupacion = this.PerfilForm.get('ocupacion').value;
      this.userObject.direccion = this.PerfilForm.get('direccion').value;
      this.userObject.ciudad = this.PerfilForm.get('ciudad').value;

      
      let data = {
        cuenta: this.constantes.getCue(),
        clave: this.constantes.getClave(),
        proceso: 6,
        consulta: {
          ip: "192.168.3.127",
          idusuario: this.loginService.idUsuario,
          idpersona: this.loginService.idpersona,
          tipodocumento: this.userObject.tipoDoc,
          documento: this.userObject.documento,
          nombre: this.userObject.nombre,
          apellido: this.userObject.apellido,
          telefono: this.userObject.telefono,
          fechanac: this.userObject.fechanac,
          lugarnac: this.userObject.lugarnac,
          escolaridad: this.userObject.escolaridad,
          ocupacion: this.userObject.ocupacion,
          direccion: this.userObject.direccion,
          ciudad: this.userObject.ciudad,
        }
      }
      this.processWebService.execProcess(data)
        .subscribe( res => {
          if( res.idresponse == 1){
            this.messageResult = "Datos Editados con éxito.";
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show(); 
          }else{
            this.messageResult = JSON.parse(res.message).nombre;
            const element = document.getElementById('myModal') as HTMLElement;
            const myModal = new Modal(element);
            myModal.show();    
          }
            
        })
    }
  
}

